<?php

namespace Auth\Form;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\Validator\EmailAddress;
use Zend\Validator\NotEmpty;

class LoginInputFilter extends InputFilter {

    public $inputFilter;

    public function __construct() {
        $isEmpty = NotEmpty::IS_EMPTY;
        $invalidEmail = EmailAddress::INVALID_FORMAT;

        $this->inputFilter = new InputFilter();
        $factory = new InputFactory();

        $this->inputFilter->add($factory->createInput(array(
                    'name' => 'login',
                    'required' => true,
                    'filters' => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim')
                    ),
                    'validators' => array(
                        array(
                            'name' => 'NotEmpty',
                            'options' => array(
                                'messages' => array(
                                    $isEmpty => 'Required field.'
                                )
                            ),
                            'break_chain_on_failure' => true
                        ),
                    ),
        )));
        $this->inputFilter->add($factory->createInput(array(
                    'name' => 'passwd',
                    'required' => true,
                    'filters' => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim')
                    ),
                    'validators' => array(
                        array(
                            'name' => 'NotEmpty',
                            'options' => array(
                                'messages' => array(
                                    $isEmpty => 'Required field.'
                                )
                            ),
                        ),
                    ),
        )));
    }

}
