<?php

namespace Auth\Form;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;

class RegisterInputFilter extends InputFilter {

    public $inputFilter;

    public function __construct() {
        $this->inputFilter = new InputFilter();
        $factory = new InputFactory();

        $this->inputFilter->add($factory->createInput(array(
                    'name' => 'login',
                    'required' => true,
                    'filters' => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim')
                    ),
                    'validators' => array(
                        array(
                            'name' => 'NotEmpty',
                            'break_chain_on_failure' => true
                        )
                    ),
        )));
        $this->inputFilter->add($factory->createInput(array(
                    'name' => 'passwd',
                    'required' => true,
                    'filters' => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim')
                    ),
                    'validators' => array(
                        array(
                            'name' => 'NotEmpty',
                            'break_chain_on_failure' => true
                        ),
                        array(
                            'name' => 'StringLength',
                            'options' => array(
                                'min' => 6
                            )
                        ),
                    ),
        )));

        $this->inputFilter->add($factory->createInput(array(
                    'name' => 'passwd2',
                    'required' => true,
                    'filters' => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim')
                    ),
                    'validators' => array(
                        array(
                            'name' => 'NotEmpty',
                            'break_chain_on_failure' => true
                        ),
                        array(
                            'name' => 'Identical',
                            'options' => array(
                                'token' => 'passwd',
                            )
                        ),
                    ),
        )));
    }

}
