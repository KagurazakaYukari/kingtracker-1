<?php

namespace Auth\Controller;

use Auth\Form\LoginForm;
use Auth\Form\LoginInputFilter;
use Auth\Form\RegisterForm;
use Auth\Form\RegisterInputFilter;
use Auth\Service\UserService;
use Exception;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class IndexController extends AbstractActionController {

    protected $userService;
    protected $auth;

    public function IndexAction() {
        $view = new ViewModel();
        $view->setTerminal(true);
        
        return $view;
    }

    public function LoginAction() {

        $view = new ViewModel();
        $view->setTerminal(true);
        $view->setTemplate('auth/index/login');

        $form = new LoginForm();

        $request = $this->getRequest();
        if ($request->isPost()) {
            $inputFilter = new LoginInputFilter();
            $form->setInputFilter($inputFilter->inputFilter);
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $data = $form->getData();

                $authAdapter = $this->auth->getAdapter();
                $authAdapter->setIdentity($data['login'])->setCredential($data['passwd']);
                $result = $this->auth->authenticate();

                if (!$result->isValid()) {
                    foreach ($result->getMessages() as $error) {
                        $this->flashMessenger()->setNamespace('error')->addMessage($error);
                    }
                    $this->plugin('redirect')->toRoute('auth/default', array('controller' => 'Index', 'action' => 'login'));
                    return false;
                } else {
                    $storage = $this->auth->getStorage();
                    $storage->write($authAdapter->getResultRowObject(array('id', 'login', 'role'), array('passwd', 'salt')));
                    $this->plugin('redirect')->toRoute('home');
                    return false;
                }
            }
        }

        return $view->setVariables(array(
                    'form' => $form
        ));
    }

    public function LogoutAction() {
        $sm = $this->getServiceLocator();
        $auth = $sm->get('AuthService');

        if ($auth->hasIdentity()) {
            $auth->clearIdentity();
        }
        $this->plugin('redirect')->toRoute('auth/default', array('controller' => 'Index', 'action' => 'login'));
        return false;
    }

    public function RegisterAction() {
        $user = $this->getServiceLocator()->get('Auth\Model\User');

        $view = new ViewModel();
        $view->setTemplate('auth/index/register');

        $form = new RegisterForm();
        $request = $this->getRequest();

        if ($request->isPost()) {
            $filter = new RegisterInputFilter();
            $form->setInputFilter($filter->inputFilter);
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $user = $this->getServiceLocator()->get('Auth\Model\User');
                $user->exchangeArray($form->getData());
                $user->save();
            }
        }

        return $view->setVariables(array(
                    'form' => $form
        ));
    }

    public function ProfileAction() {
        $view = new ViewModel();

        $userIdentity = $this->auth->getIdentity();
        $this->layout()->setVariable('user', $this->auth->getIdentity());

        try {
            $user = $this->userService->getByLogin($userIdentity->login);
        } catch (Exception $ex) {
            return $this->redirect()->toRoute('admin/default', array('controller' => 'hosts'));
        }

        $form = new RegisterForm();
        $request = $this->getRequest();

        if ($request->isPost()) {
            $post = $request->getPost();

            $inputFilter = new RegisterInputFilter();
            $form->setInputFilter($inputFilter->inputFilter);
            $form->setData($post);
            if ($form->isValid()) {
                $data = $form->getData();

                $salt = '';
                $user->login = $data['login'];
                $user->passwd = $this->userService->hashPassword($data['passwd'], $salt);
                $user->salt = $salt;
                $this->userService->save($user);

                $this->flashMessenger()->setNamespace('success')->addMessage('Your changes have been saved');
                return $this->redirect()->toRoute('auth/default', array('action' => 'profile'));
            }

            $view->setVariable('post', $post);
        }

        return $view->setVariables(array(
                    'form' => $form,
                    'user' => $user
        ));
    }

    public function setUserService(UserService $service) {
        $this->userService = $service;
    }

    public function setAuthService($service) {
        $this->auth = $service;
    }

}
