<?php

namespace Admin;

use Admin\Controller\HostsController;
use Admin\Model\Host;
use Admin\Service\HostService;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\Mvc\Controller\ControllerManager;
use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;

class Module {

    public function onBootstrap(MvcEvent $e) {
        $eventManager = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
    }

    public function getAutoloaderConfig() {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php',
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getConfig() {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getServiceConfig() {
        return array(
            'factories' => array(
                'Admin\Service\HostService' => function($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Host());
                    $tableGateway = new TableGateway('hosts', $dbAdapter, null, $resultSetPrototype);

                    $table = new HostService($tableGateway);
                    //$table->setServiceLocator($sm);
                    return $table;
                },
                'Admin\Form\AddAdminInputFilter' => function($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    return new Form\AddAdminInputFilter($dbAdapter);
                },
        ));
    }

    public function getControllerConfig() {
        return array(
            'factories' => array(
                'Admin\Controller\Hosts' => function(ControllerManager $cm) {
                    $sm = $cm->getServiceLocator();
                    $depA = $sm->get('Admin\Service\HostService');
                    $c = new HostsController($depA);
                    $c->setAuthService($sm->get('AuthService'));
                    return $c;
                },
                'Admin\Controller\Player' => function(ControllerManager $cm) {
                    $sm = $cm->getServiceLocator();
                    $depA = $sm->get('Insim\Service\PlayerService');
                    $c = new Controller\PlayerController($depA);
                    $c->setACLService($sm->get('Insim\Service\ACLService'));
                    $c->setAuthService($sm->get('AuthService'));
                    $c->setAddAdminFormFilter($sm->get('Admin\Form\AddAdminInputFilter'));
                    $c->setTaskService($sm->get('Insim\Service\TaskService'));
                    return $c;
                },
                'Admin\Controller\LFSW' => function(ControllerManager $cm) {
                    $sm = $cm->getServiceLocator();
                    $depA = $sm->get('Insim\Service\LFSWService');
                    $c = new Controller\LFSWController($depA);
                    $c->setAuthService($sm->get('AuthService'));
                    $c->setTaskService($sm->get('Insim\Service\TaskService'));
                    return $c;
                },
                'Admin\Controller\Settings' => function(ControllerManager $cm) {
                    $sm = $cm->getServiceLocator();
                    //$depA = $sm->get('Insim\Service\SeService');
                    $c = new Controller\SettingsController();
                    $c->setAuthService($sm->get('AuthService'));
                    $c->setTaskService($sm->get('Insim\Service\TaskService'));
                    $c->setParamsService($sm->get('Insim\Service\ParamsService'));
                    return $c;
                },
            ),
        );
    }

}
