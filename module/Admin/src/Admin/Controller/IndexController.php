<?php

namespace Admin\Controller;

use Zend\Mvc\Controller\AbstractActionController;

class IndexController extends AbstractActionController {

    public function indexAction() {
        //return $this->redirect()->toRoute('admin/default', array('controller' => 'insim', 'action' => 'hosts', 'lang' => 'en'));
        return $this->redirect()->toRoute('home');
    }

}