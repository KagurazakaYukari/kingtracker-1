<?php

namespace Admin\Controller;

use Admin\Controller\AdminController;
use Admin\Form\AddHostForm;
use Admin\Form\AddHostInputFilter;
use Exception;
use Insim\Model\Host;
use PDO;
use Zend\Config\Config;
use Zend\Config\Writer\PhpArray;
use Zend\Debug\Debug;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;

require_once(VENDOR_PATH . '/lfs/LFSString.php');

class InsimController extends AdminController {
    public function __construct() {
        
    }

    public function indexAction() {
        $view = new ViewModel();
        
        return $view->setVariables(array());
    }

    public function HostsAction() {
        $view = new ViewModel();
        
        return $view->setVariables(array());
    }
}
