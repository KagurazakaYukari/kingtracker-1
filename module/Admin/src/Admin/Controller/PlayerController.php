<?php

namespace Admin\Controller;

use Admin\Form\AddAdminForm;
use Admin\Form\AddAdminInputFilter;
use Auth\Controller\AdminController;
use Exception;
use Insim\Model\PlayerACL;
use Insim\Model\Task;
use Insim\Service\ACLService;
use Insim\Service\PlayerService;
use Insim\Service\TaskService;
use Zend\View\Model\ViewModel;

class PlayerController extends AdminController {

    protected $playerService;
    protected $taskService;
    protected $aclService;
    protected $addAdminInputFilter;

    public function __construct(PlayerService $playerService) {
        $this->hostService = $playerService;
    }

    public function indexAction() {
        $view = new ViewModel();

        //$hosts = $this->hostService->fetchAllBy(array(), true, false, 'name ASC');


        return $view->setVariables(array(
                        //'hosts' => $hosts,
        ));
    }

    public function adminAction() {
        $view = new ViewModel();

        return $view->setVariables(array(
                    'admins' => $this->aclService->fetchAllBy($where = array(), $buffered = true, $tableGateway = false, $order = 'UName ASC')
        ));
    }

    public function addAdminAction() {
        $view = new ViewModel();
        $view->setTemplate('admin/player/addadmin');

        $form = new AddAdminForm();
        $request = $this->getRequest();

        if ($request->isPost()) {
            $post = $request->getPost();

            $form->setInputFilter($this->addAdminInputFilter->inputFilter);
            $form->setData($post);
            if ($form->isValid()) {
                $data = $form->getData();

                $admin = new PlayerACL();
                $admin->UName = $data['UName'];
                $admin->role = $data['role'];
                $this->aclService->save($admin);
                $this->taskService->addTask(Task::TASK_ACL_UPDATE);

                $this->flashMessenger()->setNamespace('success')->addMessage('Item has been added');
                return $this->redirect()->toRoute('admin/default', array('controller' => 'player', 'action' => 'admin'));
            }

            $view->setVariable('post', $post);
        }

        return $view->setVariables(array(
                    'form' => $form,
        ));
    }
    
    public function editAdminAction() {
        $id = intval($this->getEvent()->getRouteMatch()->getParam('id'));

        try {
            $admin = $this->aclService->getByID($id);
        } catch (Exception $ex) {
            return $this->redirect()->toRoute('admin/default', array('controller' => 'player', 'action' => 'admin'));
        }

        $view = new ViewModel();
        $view->setTemplate('admin/player/addadmin');

        $form = new AddAdminForm();
        $request = $this->getRequest();

        if ($request->isPost()) {
            $post = $request->getPost();

            $this->addAdminInputFilter->UName($admin->id);
            $form->setInputFilter($this->addAdminInputFilter->inputFilter);
            $form->setData($post);
            if ($form->isValid()) {
                $data = $form->getData();

                $admin->UName = $data['UName'];
                $admin->role = $data['role'];
                $this->aclService->save($admin);
                $this->taskService->addTask(Task::TASK_ACL_UPDATE);

                $this->flashMessenger()->setNamespace('success')->addMessage('Your changes have been saved');
                return $this->redirect()->toRoute('admin/default', array('controller' => 'player', 'action' => 'admin'));
            }

            $view->setVariable('post', $post);
        }

        return $view->setVariables(array(
                    'form' => $form,
                    'admin' => $admin
        ));
    }

    public function deleteAdminAction() {
        $id = intval($this->getEvent()->getRouteMatch()->getParam('id'));

        try {
            $admin = $this->aclService->getByID($id);
        } catch (Exception $ex) {
            return $this->redirect()->toRoute('admin/default', array('controller' => 'player', 'action' => 'admin'));
        }

        $this->aclService->delete($admin->id);
        $this->taskService->addTask(Task::TASK_ACL_UPDATE);

        $this->flashMessenger()->setNamespace('success')->addMessage('Item has been deleted');
        return $this->redirect()->toRoute('admin/default', array('controller' => 'player', 'action' => 'admin'));
    }

    public function setACLService(ACLService $service) {
        $this->aclService = $service;
    }
    
    public function setAddAdminFormFilter(AddAdminInputFilter $filter){
        $this->addAdminInputFilter = $filter;
    }
    
    public function setTaskService(TaskService $serice) {
        $this->taskService = $serice;
    }

}
