<?php

namespace Admin\Form;

use Zend\Db\Adapter\Adapter;
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;

class AddAdminInputFilter extends InputFilter {

    public $inputFilter;
    protected $factory;
    protected $dbAdapter;

    public function __construct(Adapter $dbAdapter) {
        $this->dbAdapter = $dbAdapter;
        $this->inputFilter = new InputFilter();
        $this->factory = new InputFactory();

        $this->inputFilter->add($this->factory->createInput(array(
                    'name' => 'role',
                    'required' => true,
                    'filters' => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim')
                    ),
                    'validators' => array(
                        array(
                            'name' => 'NotEmpty',
                            'break_chain_on_failure' => true
                        ),
                    ),
        )));
        
        $this->inputFilter->add($this->factory->createInput(array(
                    'name' => 'UName',
                    'required' => true,
                    'filters' => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim')
                    ),
                    'validators' => array(
                        array(
                            'name' => 'NotEmpty',
                            'break_chain_on_failure' => true
                        ),
                        array(
                            'name' => 'Db\NoRecordExists',
                            'options' => array(
                                'table' => 'players_acl',
                                'field' => 'UName',
                                'adapter' => $this->dbAdapter,
                            ),
                        ),
                    ),
        )));
        
        
    }

    public function UName($exclude_id = 0) {
        $this->inputFilter->remove('UName');
        
        $this->inputFilter->add($this->factory->createInput(array(
                    'name' => 'UName',
                    'required' => true,
                    'filters' => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim')
                    ),
                    'validators' => array(
                        array(
                            'name' => 'NotEmpty',
                            'break_chain_on_failure' => true
                        ),
                        array(
                            'name' => 'Db\NoRecordExists',
                            'options' => array(
                                'table' => 'players_acl',
                                'field' => 'UName',
                                'exclude' => array(
                                    'field' => 'id',
                                    'value' => $exclude_id
                                ),
                                'adapter' => $this->dbAdapter,
                            ),
                        ),
                    ),
        )));
    }

}
