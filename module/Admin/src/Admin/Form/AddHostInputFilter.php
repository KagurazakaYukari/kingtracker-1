<?php

namespace Admin\Form;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\Validator\GreaterThan;
use Zend\Validator\NotEmpty;

class AddHostInputFilter extends InputFilter {

    public $inputFilter;
    protected $factory;

    public function __construct() {
        $this->inputFilter = new InputFilter();
        $this->factory = new InputFactory();

        $this->inputFilter->add($this->factory->createInput(array(
                    'name' => 'name',
                    'required' => true,
                    'filters' => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim')
                    ),
                    'validators' => array(
                        array(
                            'name' => 'NotEmpty',
                            'break_chain_on_failure' => true
                        ),
                    ),
        )));

        $this->inputFilter->add($this->factory->createInput(array(
                    'name' => 'ip',
                    'required' => true,
                    'filters' => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim')
                    ),
                    'validators' => array(
                        array(
                            'name' => 'NotEmpty',
                            'break_chain_on_failure' => true
                        ),
                    ),
        )));

        $this->inputFilter->add($this->factory->createInput(array(
                    'name' => 'passwd',
                    'required' => true,
                    'filters' => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim')
                    ),
                    'validators' => array(
                        array(
                            'name' => 'NotEmpty',
                            'break_chain_on_failure' => true
                        ),
                    ),
        )));

        $this->inputFilter->add($this->factory->createInput(array(
                    'name' => 'path',
                    'required' => true,
                    'filters' => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim')
                    ),
                    'validators' => array(
                        array(
                            'name' => 'NotEmpty',
                            'break_chain_on_failure' => true
                        ),
                    ),
        )));

        $this->inputFilter->add($this->factory->createInput(array(
                    'name' => 'cmd',
                    'required' => true,
                    'filters' => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim')
                    ),
                    'validators' => array(
                        array(
                            'name' => 'NotEmpty',
                            'break_chain_on_failure' => true
                        ),
                        array(
                            'name' => 'StringLength',
                            'options' => array(
                                'min' => 1,
                                'max' => 1
                            )
                        ),
                    ),
        )));
    }

}
