<?php

namespace Admin\Form;

use Zend\Form\Form;

class AddAdminForm extends Form {

    public function __construct($name = null, $options = array()) {
        parent::__construct($name, $options);

        $this->add(array(
            'name' => 'UName',
            'options' => array(
                'label' => 'Licence name',
            ),
            'attributes' => array(
                'type' => 'text',
                'class' => 'form-control'
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'role',
            'options' => array(
                'label' => 'Role',
                'value_options' => array(
                    'guest' => 'Guest',
                    'marshal' => 'Marshal',
                    'admin' => 'Admin',
                ),
            ),
            'attributes' => array(
                'class' => 'form-control'
            )
        ));
    }

}
