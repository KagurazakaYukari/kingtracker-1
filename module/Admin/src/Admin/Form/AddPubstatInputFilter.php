<?php

namespace Admin\Form;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;

class AddPubstatInputFilter extends InputFilter {

    public $inputFilter;
    protected $factory;

    public function __construct() {
        $this->inputFilter = new InputFilter();
        $this->factory = new InputFactory();

        $this->inputFilter->add($this->factory->createInput(array(
                    'name' => 'key',
                    'required' => true,
                    'filters' => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim')
                    ),
                    'validators' => array(
                        array(
                            'name' => 'NotEmpty',
                            'break_chain_on_failure' => true
                        ),
                    ),
        )));
    }

}
