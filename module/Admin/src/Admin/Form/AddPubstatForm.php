<?php

namespace Admin\Form;

use Zend\Form\Form;

class AddPubstatForm extends Form {

    public function __construct($name = null, $options = array()) {
        parent::__construct($name, $options);

        $this->add(array(
            'name' => 'key',
            'options' => array(
                'label' => 'Key',
            ),
            'attributes' => array(
                'type' => 'text',
                'class' => 'form-control'
            ),
        ));
    }
}
