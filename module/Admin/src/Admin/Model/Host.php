<?php

namespace Admin\Model;

use Application\Model\CoreModel;

class Host extends CoreModel {

    public $id;
    public $name;
    public $ip;
    public $port;
    public $passwd;
    public $path = null;
    public $command = '!';
    public $active = 1;
    public $local = 0;
    public $silent = 0;
    public $localUCID = 0;
    public $status = 0;
    public $version;
    public $licence;
    public $connected;
    public $updated;
    public $disconnect_reason;
    public $Wind;
    public $Weather;
    public $Track;
    public $TrackName = '';
    public $TrackInfo = null;
    public $lastTrack = '';
    public $trackChanged = false;
    public $reversed = false;
    public $LName;
    public $RaceLaps;
    public $QualMins;
    public $ViewPLID = 0;
    public $splitsCount = 0;
    public $timing = 0;
    public $socket = false;
    public $insim = null;
    public $playerService = null;
    public $lastPing = 0;
    public $config = array();
    public $hostConfigKeys = array(
        'canreset', 'vote', 'select', 'midrace', 'mustpit', 'fcv', 'cruise', 'autokick'
    );
    
    public function exchangeArray($data) {
        $this->id = (!empty($data['id'])) ? $data['id'] : null;
        $this->name = (!empty($data['name'])) ? $data['name'] : null;
        $this->ip = (!empty($data['ip'])) ? $data['ip'] : null;
        $this->port = (!empty($data['port'])) ? $data['port'] : null;
        $this->passwd = (!empty($data['passwd'])) ? $data['passwd'] : null;
        $this->path = (!empty($data['path'])) ? $data['path'] : null;
        $this->cmd = (!empty($data['cmd'])) ? $data['cmd'] : null;
        $this->active = $data['active'];
        $this->local = $data['local'];
        $this->silent = $data['silent'];
        $this->status = (!empty($data['status'])) ? $data['status'] : null;
        $this->version = (!empty($data['version'])) ? $data['version'] : null;
        $this->licence = (!empty($data['licence'])) ? $data['licence'] : null;
        $this->connected = (!empty($data['connected'])) ? $data['connected'] : null;
        $this->updated = (!empty($data['updated'])) ? $data['updated'] : null;
        $this->disconnect_reason = (!empty($data['disconnect_reason'])) ? $data['disconnect_reason'] : null;

        $this->Wind = (!empty($data['Wind'])) ? $data['Wind'] : null;
        $this->Track = (!empty($data['Track'])) ? $data['Track'] : null;
        $this->LName = (!empty($data['LName'])) ? $data['LName'] : null;
    }

    public function exchangeObject() {
        $data = array();
        $data['id'] = (!empty($this->id)) ? $this->id : null;
        $data['name'] = (!empty($this->name)) ? $this->name : null;
        $data['path'] = (!empty($this->path)) ? $this->path : null;
        $data['cmd'] = (!empty($this->cmd)) ? $this->cmd : null;
        $data['ip'] = (!empty($this->ip)) ? $this->ip : null;
        $data['port'] = (!empty($this->port)) ? $this->port : null;
        $data['passwd'] = (!empty($this->passwd)) ? $this->passwd : null;
        $data['active'] = $this->active;
        $data['local'] = $this->local;
        $data['silent'] = $this->silent;
        $data['status'] = (!empty($this->status)) ? $this->status : null;
        $data['version'] = (!empty($this->version)) ? $this->version : null;
        $data['licence'] = (!empty($this->licence)) ? $this->licence : null;
        $data['connected'] = (!empty($this->connected)) ? $this->connected : null;
        $data['updated'] = (!empty($this->updated)) ? $this->updated : null;
        $data['disconnect_reason'] = (!empty($this->disconnect_reason)) ? $this->disconnect_reason : null;

        $data['Wind'] = (!empty($this->Wind)) ? $this->Wind : null;
        $data['Track'] = (!empty($this->Track)) ? $this->Track : null;
        $data['LName'] = (!empty($this->LName)) ? $this->LName : null;

        return $data;
    }
}
