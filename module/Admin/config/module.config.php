<?php

use Admin\Controller\HostsController;
use Admin\Controller\HostsControllerFactory;

return array(
    'controllers' => array(
        'invokables' => array(
            'Admin\Controller\Insim' => 'Admin\Controller\InsimController',
        //'Admin\Controller\Hosts' => 'Admin\Controller\HostsController'
        ),
    ),
    'router' => array(
        'routes' => array(
            'install' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/install[/:lang]',
                    'constraints' => array(
                        'lang' => '(sk|en)?',
                    ),
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Insim',
                        'action' => 'install',
                        'lang' => 'en',
                    ),
                ),
            ),
            'admin' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/admin[/:lang]',
                    'constraints' => array(
                        'lang' => '(sk|en)?',
                    ),
                    'defaults' => array(
                        '__NAMESPACE__' => 'Admin\Controller',
                        'controller' => 'Index',
                        'action' => 'index',
                        'lang' => 'en',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'default' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '/[:controller[/:action][/:id]]',
                            'constraints' => array(
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'id' => '[0-9]+'
                            ),
                            'defaults' => array(
                            ),
                        ),
                    ),
                ),
            ),
            'home' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/',
                    'defaults' => array(
                        /*'controller' => 'Admin\Controller\Insim',
                        'action' => 'index',*/
                        'controller' => 'Admin\Controller\Hosts',
                        'action' => 'index',
                    ),
                ),
            ),
        )
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            __DIR__ . '/../view/',
        ),
        'strategies' => array(
            'ViewJsonStrategy',
        ),
    ),
    'view_helpers' => array(
        'invokables' => array(
            'FormInput' => 'Admin\Form\View\Helper\FormInput',
        ),
    ),
);
