<?php

namespace Application\Service;

use Exception;
use Insim\Types\InSimTypes;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Where;
use Zend\Db\TableGateway\TableGateway;

abstract class MyService {
    protected $tableGateway;
    public $debug = false;

    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway = $tableGateway;
    }

    public function getById($id, $tableGateway = false){
        return $this->getBy(array('id' => $id), $tableGateway);
    }


    /**
     * Get entity by ...
     * @param type $where = array()
     * @param type $tableGateway = false
     * @return type
     */
    public function getBy($where = array(), $tableGateway = false) {
        $table = $tableGateway ? $tableGateway : $this->tableGateway;

        $rowset = $table->select($where);
        $row = $rowset->current();
        if (!$row) {
            throw new Exception('Item not found');
        }
        return $row;
    }

    public function fetchAll($buffered = false, $tableGateway = false) {
        $table = $tableGateway ? $tableGateway : $this->tableGateway;
        $resultSet = $table->select();

        if ($buffered)
            $resultSet->buffer();

        return $resultSet;
    }

    public function fetchAllBy($where = array(), $buffered = false, $tableGateway = false, $order = null) {
        $table = $tableGateway ? $tableGateway : $this->tableGateway;
        $resultSet = $table->select(function(Select $select) use ($where, $order) {
            foreach($where as $key => $value){
                $select->where->and->equalTo($key, $value);
            }
            
            if($order)
                $select->order($order);
        });

        if ($buffered)
            $resultSet->buffer();
        return $resultSet;
    }

    /**
     * Save object to DB
     * @param type $object
     * @param type $tableGateway
     * @return type
     */
    public function save($object, $tableGateway = false) {
        $table = $tableGateway ? $tableGateway : $this->tableGateway;
        $data = $object->exchangeObject();

        $id = (int) $object->id;
        if ($id == 0) {
            $table->insert($data);
            return $table->lastInsertValue;
        } else {
            $table->update($data, array('id' => $id));
        }
    }

    public function delete($id) {
        $where = new Where();
        $where->equalTo('id', (int) $id);

        $this->tableGateway->delete($where);
    }

    public function setDebug($debug) {
        $this->debug = $debug;
    }

    public function log($message, $time = true) {
        $message = ($time ? "[" . (date(InSimTypes::LOG_DATE_FORMAT)) . "] - " : "") . $message . PHP_EOL;

        if ($this->debug)
            echo $message;
    }

    public function toArray($resultSet) {
        $return = array();

        foreach ($resultSet as $res) {
            $return[] = $res;
        }

        return $return;
    }
}
