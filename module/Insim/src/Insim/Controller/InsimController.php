<?php

namespace Insim\Controller;

use Application\Controller\CoreController;
use Insim\Model\Task;
use Insim\Service\ACLService;
use Insim\Service\CommandService;
use Insim\Service\HostService;
use Insim\Service\HostSettingsService;
use Insim\Service\InsimService;
use Insim\Service\LapsService;
use Insim\Service\LFSWService;
use Insim\Service\ParamsService;
use Insim\Service\PlayerService;
use Insim\Service\PlayerSettingsService;
use Insim\Service\RequestsService;
use Insim\Service\TaskService;
use Insim\Service\TrackService;
use Insim\Service\UIService;
use Insim\Service\UpdateService;
use Insim\Types\InSimTypes;
use RuntimeException;
use Zend\Console\Request as ConsoleRequest;
use const VERSION;

class InsimController extends CoreController {

    public $insimService;
    public $paramsService;
    public $trackService;
    public $hostService;
    public $hostSettingsService;
    public $playerService;
    public $messageService;
    public $taskService;
    public $requestsService;
    public $ACLService;
    public $UIService;
    public $commandService;
    public $lapsService;
    public $timecompareService;
    public $LFSWService;
    public $playerSettingsService;
    public $updateService;
    public $resultService;
    public $carService;
    public $safetyService;
    public $layoutService;
    public $translator;
    protected $hosts;
    protected $isRunning;
    protected $debug;
    private $tLastDebug;

    public function __construct() {
        //error_reporting(E_ERROR | E_WARNING | E_PARSE | E_NOTICE);
    }

    /**
     * Start InSim server
     * @return type
     * @throws RuntimeException
     */
    public function StartAction() {
        ini_set('max_execution_time', 0);

        $this->debug = false;
        $request = $this->getRequest();

        if (!$request instanceof ConsoleRequest) {
            throw new RuntimeException('You can only use this action from a console!');
        }

        $this->debug = $request->getParam('debug') || $request->getParam('d');
        if ($this->debug) {
            $this->log('======================================================================', false);
            $this->log('   KingTracker ' . VERSION, false);
            $this->log('======================================================================', false);
        }

        $this->hosts = array();
        $this->hostService->hosts = &$this->hosts;
        foreach ($this->hostService->fetchAll() as $host) {
            $host->insim = clone $this->insimService;
            $host->insim->setHostService($this->hostService);
            $host->insim->setRequestService($this->requestsService);
            $host->insim->setPlayersService($this->playerService);
            $host->insim->connect($host);
            $this->hosts[$host->id] = $host;
        }

        $this->isRunning = true;
        $this->Run();
    }

    public function UpdateAction() {
        ini_set('max_execution_time', 0);

        $this->debug = true;
        $request = $this->getRequest();

        if (!$request instanceof ConsoleRequest) {
            throw new RuntimeException('You can only use this action from a console!');
        }

        $this->log('======================================================================', false);
        $this->log('   KingTracker ' . VERSION, false);
        $this->log('======================================================================', false);
        $this->log('Update started');

        //$this->getServiceLocator()->get('Insim\Service\UpdateService')->installUpdates();
    }

    /**
     * Run loop
     */
    public function Run() {
        $tLastTick = microtime(true);
        $tLastReconnect = time();
        $tLastTasks = time();

        while ($this->isRunning === true) {
            // Reconnect disconnected host
            if ((time() - $tLastReconnect) >= 30) {
                foreach ($this->hosts as $host) {
                    if ($host->status != InSimTypes::CONN_CONNECTED) {
                        $host->insim->connect($host);
                    }
                }
                $tLastReconnect = time();
            }

            // Hosts tick
            foreach ($this->hosts as $host) {
                $host->insim->run();
            }

            // Check for Task
            if ((time() - $tLastTasks) >= 10) {
                $tasks = $this->taskService->fetchAll();
                if (count($tasks) > 0) {
                    foreach ($tasks as $task) {
                        $this->processTask($task);
                    }
                }

                $tLastTasks = time();
            }

            // Requests processing
            $this->requestsService->run();

            if ((time() - $this->tLastDebug) >= 1) {
                //$this->debug();

                $this->tLastDebug = time();
            }

            $execTime = (microtime(true) - $tLastTick) * 1000000;
            $delayTime = 50 - $execTime;
            $delayTime = $delayTime < 0 ? 0 : $delayTime;

            //echo "tick: {$execTime} delay: {$delayTime}".PHP_EOL;
            usleep($delayTime);
            $tLastTick = microtime(true);
        }
    }

    public function processTask($task) {
        switch ($task->task) {
            case Task::TASK_ACL_UPDATE:
                break;
            case Task::TASK_LFSW_UPDATE_PUBSTAT:
                $this->LFSWService->getKeys();
                break;
        }

        $this->log("Task: {$task->task}");
        $this->taskService->delete($task->id);
    }

    public function log($message, $time = true) {
        $message = ($time ? "[" . (date(InSimTypes::LOG_DATE_FORMAT)) . "] - " : "") . $message . PHP_EOL;

        if ($this->debug)
            echo $message;
    }

    public function debug() {
        for ($i = 0; $i < 40; $i++)
            echo "\r\n";

        foreach ($this->hosts as $host) {
            echo "host: ID: {$host->id}, {$host->name}" . PHP_EOL;
            echo "players: " . count(@$host->playerService->players[$host->id]) . PHP_EOL;
            echo "timers: " . count($host->timerService->timers) . PHP_EOL;
            if (count($host->timerService->timers)) {
                foreach ($host->timerService->timers as $timer) {
                    switch ($timer->state) {
                        case \Insim\Model\Timer::TIMER_RUNNING: $state = 'RUNNING';
                            break;
                        case \Insim\Model\Timer::TIMER_FINISHED: $state = 'RUNNING';
                            break;
                        default: $state = '';
                            break;
                    }

                    echo "   - {$timer->name} ({$timer->uid}): {$state} {$timer->timer} " . PHP_EOL;
                }
            }

            echo PHP_EOL . PHP_EOL;
        }
    }

    // Services injection
    public function setInsimService(InsimService $service) {
        $this->insimService = $service;
    }

    public function setTranslatorService(\Insim\Service\TranslatorService $service) {
        $this->translator = $service;
        $this->translator->init();
    }

    public function setPlayerSettingsService(PlayerSettingsService $service) {
        $this->playerSettingsService = $service;
    }

    public function setHostSettingsService(HostSettingsService $service) {
        $this->hostSettingsService = $service;
    }

    public function setLFSWService(LFSWService $service) {
        $this->LFSWService = $service;
    }

    public function setTrackService(TrackService $service) {
        $this->trackService = $service;
        $this->LFSWService = $this->LFSWService;
    }

    public function setLayoutService(\Insim\Service\LayoutService $service) {
        $this->layoutService = $service;
    }

    public function setParamsService(ParamsService $service) {
        $this->paramsService = $service;
    }

    public function setTaskService(TaskService $service) {
        $this->taskService = $service;
    }

    public function setACLService(ACLService $service) {
        $this->ACLService = $service;
    }

    public function setCommandService(CommandService $service) {
        $this->commandService = $service;
    }

    public function setTimeCompareService(\Insim\Service\TimeCompareService $service) {
        $this->timecompareService = $service;
        $this->timecompareService->LFSWService = $this->LFSWService;
    }
    
    public function setLapsService(LapsService $service) {
        $this->lapsService = $service;
        $this->lapsService->LFSWService = $this->LFSWService;
    }

    public function setHostService(HostService $service) {
        $this->hostService = $service;
        $this->hostService->trackService = $this->trackService;
        $this->hostService->hostSettingsService = $this->hostSettingsService;
        $this->hostService->resultService = $this->resultService;
        $this->hostService->safetyService = $this->safetyService;
        $this->hostService->carService = $this->carService;
        $this->hostService->layoutService = $this->layoutService;
        $this->hostService->lapsService = $this->lapsService;
    }
    
    public function setUpdateService(UpdateService $service) {
        $this->updateService = $service;
        $this->updateService->paramsService = $this->paramsService;
    }

    public function setUIService(UIService $service) {
        $this->UIService = $service;
        $this->UIService->LFSWService = $this->LFSWService;
        $this->UIService->lapsService = $this->lapsService;
        $this->UIService->trackService = $this->trackService;
    }

    public function setRequestsService(RequestsService $service) {
        $this->requestsService = $service;
        $this->requestsService->LFSWService = $this->LFSWService;
        $this->requestsService->updateService = $this->updateService;
    }

    public function setPlayersService(PlayerService $service) {
        $this->playerService = $service;
        $this->playerService->ACLService = $this->ACLService;
        $this->playerService->UIService = $this->UIService;
        $this->playerService->commandService = $this->commandService;
        $this->playerService->lapsService = $this->lapsService;
        $this->playerService->timecompareService = $this->timecompareService;
        $this->playerService->playerSttingsService = $this->playerSettingsService;
        $this->playerService->safetyService = $this->safetyService;
        $this->playerService->translator = $this->translator;
    }

    public function setResultService(\Insim\Service\ResultService $service) {
        $this->resultService = $service;
    }

    public function setCarService(\Insim\Service\CarService $service) {
        $this->carService = $service;
    }

    public function setSafetyService(\Insim\Service\SafetyService $service) {
        $this->safetyService = $service;
    }

}
