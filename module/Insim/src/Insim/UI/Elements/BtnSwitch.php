<?php

namespace Insim\UI\Elements;

use Insim\Model\PlayerClass;
use Insim\Packets\isBTC;
use Insim\Packets\isBTN;
use Insim\Types\ButtonStyles;
use Zend\Debug\Debug;

class BtnSwitch extends Element {
    public $current;
    public $values;
    public $values_count = 0;
    public $button_id_value = 0;
    public $key_value = -1;
    public $width = 10;
    public $width_label = 40;
            
    function __construct(PlayerClass $player, $label, $height = 5, $top = 0, $left = 0) {
        parent::__construct($player, $label, $height, $top, $left);
    }

    function show($id_current, $top = 0, $left = 0) {
        parent::show($id_current, $top, $left);
        
        $button = new isBTN();

        $button->ReqI = ++$id_current;
        $button->ClickID = $button->ReqI;
        $button->L = $this->left + 1;
        $button->T = $this->top;
        $button->H = $this->height;
        $button->W = $this->width_label;
        $button->BStyle = ButtonStyles::ISB_RIGHT;
        $button->Text = '^7'.$this->label;

        $this->addButton($button);
        $this->send($button);
        
        $button->ReqI = ++$id_current;
        $button->ClickID = $button->ReqI;
        $button->L = $this->left + 1 + $button->W;
        $button->T = $this->top;
        $button->H = $this->height;
        $button->W = $this->width;
        $button->BStyle = ButtonStyles::ISB_DARK + ButtonStyles::ISB_CLICK;
        $button->Text = isset($this->values[$this->key_value]) ? $this->values[$this->key_value]['value'] : '^8---';

        $this->button_id_value = $button->ReqI;
        $this->addButton($button, 'clickChangeValue');
        $this->send($button);

        $this->end_id = $id_current;
    }
    
    function setValues($values, $current = ''){     
        $index = 0;
        foreach($values as $key => $value){
            $this->values[$index] = array(
                'key' => $key,
                'value' => $value
            );
            
            if($key == $current){
                $this->key_value = $index;
            }
            
            $index++;
        }
        
        $this->values_count = count($values);
        $this->current = $current;
    }
    
    public function eventClick(isBTC $packet) {
        parent::eventClick($packet);
        
        if (isset($this->buttons[$packet->ClickID])) {
            if (isset($this->buttons[$packet->ClickID]['callBack'])) {
                $callBack = $this->buttons[$packet->ClickID]['callBack'];
                $this->{$callBack}();
            }
        }
    }
    
    function clickChangeValue(){
        $this->key_value++;
        if($this->key_value > ($this->values_count - 1)){
            $this->key_value = 0;
        }
        
        // Update button
        $button = new isBTN();
        $button->ReqI = $this->button_id_value;
        $button->ClickID = $this->button_id_value;
        $button->Text = $this->values[$this->key_value]['value'];
        $this->send($button);
        
        if($this->eventValueChanged){
            call_user_func($this->eventValueChanged, $this->values[$this->key_value]['key'], $this->values[$this->key_value]['value']);
        }
    }

}
