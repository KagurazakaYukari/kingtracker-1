<?php

namespace Insim\UI\Elements;

use Insim\Model\PlayerClass;
use Insim\Packets\isBTN;
use Insim\Packets\isBTT;
use Insim\Types\ButtonStyles;
use Insim\Types\MsgTypes;

class BtnInput extends Element {

    public $uuid;
    public $current;
    public $values;
    public $values_count = 0;
    public $button_id_value = 0;
    public $key_value = -1;
    public $width = 10;

    function __construct(PlayerClass $player, $label = '', $height = 5, $top = 0, $left = 0, $width = 10, $uuid = null) {
        parent::__construct($player, $label, $height, $top, $left);
        $this->width = $width;
        $this->uuid = $uuid;
    }

    function show($id_current, $top = 0, $left = 0) {
        parent::show($id_current, $top, $left);

        $button = new isBTN();

        if (!empty($this->label)) {
            $button->ReqI = ++$id_current;
            $button->ClickID = $button->ReqI;
            $button->L = $this->left + 1;
            $button->T = $this->top;
            $button->H = $this->height;
            $button->W = 40;
            $button->BStyle = ButtonStyles::ISB_RIGHT;
            $button->Text = '^7' . $this->label;

            $this->addButton($button);
            $this->send($button);

            $lft = $this->left + 1 + $button->W;
        } else {
            $lft = $this->left + 1;
        }

        foreach ($this->values as $key => $value) {
            $button->ReqI = ++$id_current;
            $button->ClickID = $button->ReqI;
            $button->L = $lft;
            $button->T = $this->top;
            $button->H = $this->height;
            $button->W = $this->width;
            $button->BStyle = ButtonStyles::ISB_DARK + ButtonStyles::ISB_CLICK;
            $button->TypeIn = 32;
            $button->Text = $value['value'];

            $this->values[$key]['id'] = $button->ReqI;
            $this->addButton($button, 'typeChangeValue');
            $this->send($button);

            $lft += $button->W;
        }

        $this->end_id = $id_current;
    }

    /**
     * Redraw 
     * @param type $newValues
     */
    function redraw($newValues) {
        foreach ($this->values as $value) {
            if (isset($newValues[$value['key']])) {
                $button = new isBTN();
                $button->ReqI = $value['id'];
                $button->ClickID = $value['id'];
                $button->Text = $newValues[$value['key']];
                $this->send($button);
            }
        }
    }

    function setValues($values) {
        $index = 0;
        foreach ($values as $key => $value) {
            $this->values[$index] = array(
                'id' => 0,
                'key' => $key,
                'value' => $value
            );

            $index++;
        }
        $this->values_count = count($values);
    }

    public function eventType(isBTT $packet) {
        parent::eventType($packet);

        if (isset($this->buttons[$packet->ClickID])) {
            if (isset($this->buttons[$packet->ClickID]['callBack'])) {
                $callBack = $this->buttons[$packet->ClickID]['callBack'];
                foreach ($this->values as $val) {
                    if ($val['id'] == $packet->ClickID) {
                        $this->{$callBack}($val, $packet);
                        break;
                    }
                }
            }
        }
    }

    function typeChangeValue($value, isBTT $packet) {
        if ($this->eventValueChanged && isset($value['id']) && $value['id']) {
            if ($this->uuid) {
                $return = call_user_func($this->eventValueChanged, $value['key'], $packet->Text, $this->uuid);
            } else {
                $return = call_user_func($this->eventValueChanged, $value['key'], $packet->Text);
            }

            $button = new isBTN();
            $button->ReqI = $value['id'];
            $button->ClickID = $value['id'];
            $button->Text = MsgTypes::WHITE . $return;
            $this->send($button);
        }
    }

    function clickChangeValue() {
        $this->key_value++;
        if ($this->key_value > ($this->values_count - 1)) {
            $this->key_value = 0;
        }

        // Update button
        $button = new isBTN();
        $button->ReqI = $this->button_id_value;
        $button->ClickID = $this->button_id_value;
        $button->Text = $this->values[$this->key_value]['value'];
        $this->send($button);

        if ($this->eventValueChanged) {
            call_user_func($this->eventValueChanged, $this->values[$this->key_value]['key']);
        }
    }

}
