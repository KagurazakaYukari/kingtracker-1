<?php

namespace Insim\UI\Elements;

use Insim\Model\PlayerClass;
use Insim\Packets\isBTC;
use Insim\Packets\isBTN;
use Insim\Packets\isBTT;

class Element {

    protected $player;
    protected $id_current = 0;
    public $end_id = 0;
    public $start_id = 0;
    public $top = 0;
    public $top_offset = 0;
    public $left = 0;
    public $left_offset = 0;
    public $height = 0;
    protected $buttons;
    public $label;
    public $eventValueChanged = null;
            
    function __construct(PlayerClass $player, $label, $height = 5, $top = 0, $left = 0) {
        $this->player = $player;
        $this->top_offset = $top;
        $this->left_offset = $left;
        $this->height = $height;
        $this->label = $label;
    }

    function show($id_current, $top = 0, $left = 0) {
        $this->start_id = $id_current;
        $this->top = $top + $this->top_offset;
        $this->left = $left + $this->left_offset;
    }

    function hide() { }

    public function eventClick(isBTC $packet) { }
    public function eventType(isBTT $packet){ }
    
    public function addButton(isBTN $button, $callBack = null) {
        $this->buttons[$button->ReqI] = array(
            'button' => $button,
            'callBack' => $callBack
        );
    }

    public function send($packet) {
        $packet->UCID = $this->player->UCID;
        $this->player->host->insim->sendQued(clone $packet, $this->player);
    }

}
