<?php

namespace Insim\UI;

use Insim\Model\PlayerClass;
use Insim\Packets\isBTC;
use Insim\Packets\isBTN;
use Insim\Packets\isBTT;
use Insim\Types\ButtonStyles;
use Insim\Types\GridModes;
use Insim\Types\MsgTypes;

class UIGrid extends UI {

    public $columns = array();
    public $rows = array();
    public $cars = array();
    public $filter = array();
    public $search = array();
    public $topActions = array();
    public $own_time = array();
    public $own_buttons = array();
    public $rows_buttons = array();
    public $rows_per_page = 20;
    public $items_per_page = 20;
    public $show_help = false;
    protected $_title;

    function __construct($alias, PlayerClass &$player, $width = 122, $height = 101, $top = 30) {
        $this->width = $width;
        $this->height = $height;
        $this->top = $top;
        $this->left = (200 - $width) / 2;

        $this->_title = 'Starting grid';

        // columns
        $this->columns = array(
            'number' => array('width' => 10, 'name' => 'CMD', 'style' => ButtonStyles::ISB_LIGHT),
            'name' => array('width' => 50, 'name' => 'CMD', 'style' => ButtonStyles::ISB_LEFT + ButtonStyles::ISB_DARK),
        );

        parent::__construct($alias, $player);
    }

    public function setData() {
        $this->rows = array();

        $grid = $this->player->host->hostService->generateGrid();
        $grid_mode = $this->player->host->settings->get('race-grid-mode');
        $grid_reverse = $this->player->host->settings->get('race-grid-reverse');

        switch ($grid_mode) {
            case GridModes::FINISH:
                $this->title = "{$this->_title} - last race results";
                break;
            case GridModes::RANDOM:
                $this->title = "{$this->_title} - random";
                break;
            case GridModes::REVERSE:
                $this->title = $grid_reverse == 0 ? "{$this->_title} - reverse ALL" : "{$this->_title} - reverse TOP {$grid_reverse}";
                break;
        }

        $index = 0;
        $start = $this->current_page == 1 ? 0 : ($this->current_page - 1) * $this->rows_per_page;
        $end = $start + $this->rows_per_page - 1;

        $number = 1;
        foreach ($grid as $pos => $player) {
            if ($index >= $start && $index <= $end) {
                $this->rows[] = array('number' => MsgTypes::WHITE . $number, 'name' => "^8[{$player['car']}] {$player['PName']}");
            }

            $index++;
            $number++;
        }

        $this->max_page = ceil(count($grid) / $this->rows_per_page);
        $this->status_line = $this->current_page . ' / ' . $this->max_page;
    }

    public function show($showBase = true) {
        $this->setData();

        if ($showBase)
            $this->showBase();

        $this->button_content_min = $this->id_current + 1;

        $button = new isBTN();
        $newLine = 0;
        $left = $this->left + 5;
        $index = 0;
        foreach ($this->rows as $key => $row) {
            $rowIndex = 0;
            $width = 0;
            foreach ($this->columns as $keyCol => $column) {
                $button->ReqI = ++$this->id_current;
                $button->ClickID = $button->ReqI;
                $button->L = !$rowIndex ? ($left) + 1 : $button->L + $button->W;
                $button->T = $this->top + 2 + 14 + $newLine;
                $button->H = 6;
                $button->W = $column['width'];
                $button->BStyle = ButtonStyles::COLOUR_LIGHT_GREY + (isset($column['style']) ? $column['style'] : 0);
                $button->Text = isset($row[$keyCol]) ? $row[$keyCol] : '';

                $this->send($button);
                $this->rows_buttons[$key][$keyCol] = $button->ClickID;
                $rowIndex++;
                $width += $button->W;
            }

            $newLine += 4;
            $left = $index % 2 ? ($this->left + 5) : 100;
            $index++;
        }


        $this->showFooter(-2);


        $this->button_content_max = $this->id_current;

        parent::show();
    }

    public function redrawContent() {
        $this->setData();

        $button = new isBTN();
        $button->ClickID = $this->button_id_status_line;
        $button->Text = $this->status_line;
        $this->send($button);

        foreach ($this->rows as $row_id => $row) {
            $buttons = $this->rows_buttons[$row_id];

            foreach ($buttons as $key => $id) {
                $button->ClickID = $id;
                $button->Text = $this->rows[$row_id][$key];
                $this->send($button);
            }
        }

        parent::redrawContent();
    }

    public function eventClick(isBTC $packet) {
        parent::eventClick($packet);
    }

    public function eventType(isBTT $packet) {
        parent::eventType($packet);
    }

}
