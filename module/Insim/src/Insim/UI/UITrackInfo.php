<?php

namespace Insim\UI;

use Insim\Model\PlayerClass;
use Insim\Packets\isBTC;
use Insim\Packets\isBTN;
use Insim\Packets\isBTT;
use Insim\Types\ButtonStyles;

/**
 * Small track info
 */
class UITrackInfo extends UI {

    public $id_track = 0;
    public $text;

    function __construct($alias, PlayerClass &$player) {
        $id_start = 1;
        foreach ($player->ui->ui_list_perm as $key => $ui) {
            if ($ui->displayed) {
                $id_start = $ui->id_end + 1;
            }
        }

        $id_end = $id_start + 3;

        parent::__construct($alias, $player, ($id_start), $id_end);
    }

    public function setData() {
        $this->width = 15;
        $this->height = 4;
        $this->top = 13;
        $this->left = 161;
        
        $this->text = $this->player->host->Track;
        if($this->player->host->settings->get('track-rotation')){
            $this->text .= " {$this->player->host->finishedCount}/{$this->player->host->settings->get('rotation-races')}";
        }
    }

    public function update() {   
        $button = new isBTN();
        $button->ReqI = $this->id_track;
        $button->ClickID = $this->id_track;
        $button->BStyle = ButtonStyles::COLOUR_WHITE;
        $this->text = (!empty($this->player->host->LShortName) ? $this->player->host->LShortName : $this->player->host->Track);
        if($this->player->host->settings->get('track-rotation')){
            $this->text .= " {$this->player->host->finishedCount}/{$this->player->host->settings->get('rotation-races')}";
        }
        
        $button->Text = $this->text;
        $this->send($button);
    }

    public function show() {
        $this->setData();

        $button = new isBTN();
        $button->ReqI = ++$this->id_current;
        $button->ClickID = $button->ReqI;
        $button->L = $this->left;
        $button->T = $this->top;
        $button->H = $this->height;
        $button->W = $this->width;
            
        // Click off - caused mouse always visible
        $button->BStyle = ButtonStyles::COLOUR_WHITE + ButtonStyles::ISB_RIGHT;
        $button->Text = $this->text;
        $this->id_track = $button->ClickID;
        
        $this->send($button);

        $this->button_content_max = $this->id_current;
        $this->end = $this->button_content_max;

        parent::show();
    }

    public function eventClick(isBTC $packet) {
        parent::eventClick($packet);

        if ($packet->ClickID == $this->id_track) {
            if (!isset($this->player->ui->ui_list['track']))
                $this->player->ui->showTrack();
        }
    }

    public function eventType(isBTT $packet) {
        parent::eventType($packet);
    }

    public function close() {
        parent::close();
    }

}
