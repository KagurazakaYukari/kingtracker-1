<?php

namespace Insim\UI;

use Insim\Model\HostConn;
use Insim\Model\Player;
use Insim\Model\PlayerClass;
use Insim\Packets\isBTC;
use Insim\Packets\isBTN;
use Insim\Packets\isBTT;
use Insim\Types\ButtonStyles;
use Insim\Types\InSimLang;
use Zend\Debug\Debug;
use Zend\Paginator\Adapter\Iterator as PaginatorIterator;
use Zend\Paginator\Paginator;

class UIRecentPlayers extends UI {

    public $columns = array();
    public $rows = array();
    public $rows_buttons = array();
    public $rows_per_page = 18;
    public $items_per_page = 18;
    public $show_help = false;

    function __construct($alias, PlayerClass &$player, $width = 0, $height = 0, $top = 0) {
        $this->width = $width;
        $this->height = $height;
        $this->top = $top;
        $this->left = (200 - $width) / 2;

        // columns
        $this->columns = array(
            'name' => array('width' => 75, 'name' => 'Player name / Licence name', 'style' => ButtonStyles::ISB_LEFT),
            'date' => array('width' => 23, 'name' => 'Date', 'style' => 0),
            'unban' => array('width' => 8, 'name' => '', 'style' => 0),
        );

        // Help text
        $helpText = '^7Help';

        foreach (preg_split("/((\r?\n)|(\r\n?))/", $helpText) as $line) {
            $this->help_lines[] = $line;
        }

        parent::__construct($alias, $player);
    }

    public function setData() {
        // Clear rowa
        $this->rows = array();

        for ($i = 0; $i < $this->rows_per_page; $i++) {
            $temp = array();
            foreach ($this->columns as $key => $col) {
                $temp[$key] = '';
            }
            $this->rows[] = $temp;
        }

        $paginator = new Paginator(new PaginatorIterator($this->player->playerService->getRecentPlayers()));
        $paginator->setCurrentPageNumber($this->current_page)
                ->setItemCountPerPage($this->items_per_page)
                ->setPageRange(8);

        $this->max_page = count($paginator);
        $this->status_line = $this->current_page . ' / ' . $this->max_page;

        $index = 0;
        foreach ($paginator as $recent) {
            $this->rows[$index] = array(
                'UName' => $recent->UName,
                'name' => "{$recent->PName} ^8[{$recent->UName}] ^7- {$recent->reason_string}",
                'date' => "^7{$recent->date}",
                'unban' => ($recent->reason == \Insim\Packets\isCNL::LEAVR_BANNED && $this->player->acl->check('unban')) ? '^7Unban' : ''
            );
            $index++;
        }
    }

    public function show($showBase = true) {
        $this->setData();

        if ($showBase)
            $this->showBase();

        $this->button_content_min = $this->id_current + 1;
        $this->showFooter();

        $button = new isBTN();
        $newLine = 0;
        foreach ($this->rows as $key => $row) {
            $rowIndex = 0;
            foreach ($this->columns as $keyCol => $column) {
                $button->ReqI = ++$this->id_current;
                $button->ClickID = $button->ReqI;
                $button->L = !$rowIndex ? $this->left + 1 : $button->L + $button->W;
                $button->T = $this->top + 2 + 12 + $newLine;
                $button->H = 5;
                $button->W = $column['width'];
                $button->BStyle = ButtonStyles::ISB_CLICK + ButtonStyles::ISB_DARK + ButtonStyles::COLOUR_LIGHT_GREY + (isset($column['style']) ? $column['style'] : 0);
                $button->Text = isset($row[$keyCol]) ? $row[$keyCol] : '';
                if (isset($column['typein'])) {
                    $button->TypeIn = $column['typein'];
                } else {
                    $button->TypeIn = false;
                }

                if ($keyCol == 'k' OR $keyCol == 'b' OR $keyCol == 'a') {
                    // Clear own action buttons
                    if ($this->player->UCID == @$row['ucid']) {
                        $button->Text = '';
                        $button->TypeIn = false;
                    }

                    // Access rights
                    if ($keyCol == 'k' && !$this->player->acl->check('kick')) {
                        $button->Text = '';
                        $button->TypeIn = false;
                    }
                    if ($keyCol == 'b' && !$this->player->acl->check('ban')) {
                        $button->Text = '';
                        $button->TypeIn = false;
                    }
                    if ($keyCol == 'a' && !($this->player->acl->check('spec'))) {
                        $button->Text = '';
                        $button->TypeIn = false;
                    }
                }

                $this->send($button);
                $this->rows_buttons[$key][$keyCol] = $button->ClickID;
                $rowIndex++;
            }
            $newLine += 5;
        }

        $this->button_content_max = $this->id_current;

        parent::show();
    }

    public function eventClick(isBTC $packet) {
        parent::eventClick($packet);

        foreach ($this->rows_buttons as $row_id => $buttons) {
            foreach ($buttons as $keyCol => $buttonID) {
                if ($buttonID == $packet->ClickID) {

                    if ($keyCol == 'unban') {
                        $eventInfo = array(
                            'by_id' => $this->player->player_id,
                            'by_ucid' => $this->player->UCID,
                            'UName' => $this->rows[$row_id]['UName'],
                        );

                        if ($this->player->acl->check('unban')) {
                            $this->player->playerService->unban($eventInfo);
                        }
                    }
                }
            }
        }
    }

    public function eventType(isBTT $packet) {
        parent::eventType($packet);

        foreach ($this->rows_buttons as $row_id => $buttons) {
            foreach ($buttons as $keyCol => $buttonID) {
                if ($buttonID == $packet->ClickID) {
                    // Ignore own events
                    if ($packet->UCID == $this->rows[$row_id]['ucid'])
                        continue;

                    $eventInfo = array(
                        'by_id' => $this->player->player_id,
                        'by_ucid' => $this->player->UCID,
                        'UCID' => $this->rows[$row_id]['ucid'],
                        'PLID' => $this->rows[$row_id]['plid'],
                        'UName' => $this->rows[$row_id]['UName'],
                        'typein' => strtolower(trim($packet->Text))
                    );
                    switch ($keyCol) {
                        case 'b':
                            if ($this->player->acl->check('ban')) {
                                $eventInfo['duration'] = intval(trim($eventInfo['typein']));
                                $this->player->playerService->ban($eventInfo);
                            }
                            break;
                        case 'a':
                            switch ($eventInfo['typein']) {
                                case 'spec':
                                case 's':
                                    if ($this->player->acl->check('spec')) {
                                        $this->player->playerService->spec($eventInfo);
                                    }
                                    break;
                                case 'dt':
                                    break;
                                case '30':
                                    break;
                            }
                            break;
                    }
                    return;
                }
            }
        }
    }

    public function redrawContent() {
        $this->setData();

        $button = new isBTN();
        $button->ClickID = $this->button_id_status_line;
        $button->Text = $this->status_line;
        $this->send($button);

        foreach ($this->rows as $row_id => $row) {
            $buttons = $this->rows_buttons[$row_id];

            foreach ($buttons as $key => $id) {
                $button->ClickID = $id;
                $button->Text = $this->rows[$row_id][$key];
                $this->send($button);
            }
        }

        parent::redrawContent();
    }

    public function close() {
        parent::close();
    }

}
