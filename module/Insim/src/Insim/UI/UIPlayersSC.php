<?php

namespace Insim\UI;

use Insim\Model\PlayerClass;
use Insim\Packets\isBTC;
use Insim\Packets\isBTN;
use Insim\Packets\isMTC;
use Insim\Types\ButtonStyles;
use Insim\Types\MsgTypes;

class UIPlayersSC extends UI {

    public $columns = array();
    public $rows = array();
    public $cars = array();
    public $filter = array();
    public $search = array();
    public $topActions = array();
    public $own_time = array();
    public $own_buttons = array();
    public $rows_buttons = array();
    public $rows_per_page = 16;
    public $items_per_page = 16;
    public $content_left_offset = 0;
    public $show_help = false;

    function __construct($alias, PlayerClass &$player, $width = 0, $height = 0, $top = 0) {
        $this->width = $width;
        $this->height = $height;
        $this->top = $top;
        $this->left = (200 - $width) / 2;
        $this->title = 'Best theoretical lap in current race';

        // columns
        $this->columns = array(
            'n' => array('width' => 6, 'name' => 'N', 'style' => ButtonStyles::ISB_RIGHT),
            'lap' => array('width' => 12, 'name' => 'Lap time', 'style' => ''),
            'diff' => array('width' => 8, 'name' => 'Diff', 'style' => ''),
            'name' => array('width' => 36, 'name' => 'Driver name', 'style' => ButtonStyles::ISB_LEFT),
            'splits' => array('width' => 58, 'name' => 'Splits', 'style' => ButtonStyles::ISB_LEFT),
        );

        // Top actions
        $this->topActions = array();

        parent::__construct($alias, $player);
    }

    public function setData() {
        $sc = $this->player->playerService->raceStats->getTheoreticalBest();

        // Clear rows
        $this->rows = array();

        for ($i = 0; $i < $this->rows_per_page; $i++) {
            $temp = array();
            foreach ($this->columns as $key => $col) {
                $temp[$key] = '';
            }
            $this->rows[] = $temp;
        }

        $index = 0;
        $start = $this->current_page == 1 ? 0 : ($this->current_page - 1) * $this->rows_per_page;
        $end = $start + $this->rows_per_page - 1;

        $ownTime = false;
        foreach ($sc as $player) {
            $number = (($this->current_page - 1) * $this->items_per_page) + ($index + 1);
            if ($player['PLID'] == $this->player->PLID) {
                $ownTime = $player;
                $ownTime['number'] = $number;
            }

            if ($index >= $start && $index <= $end) {
                $this->rows[$index] = array(
                    'n' => $number . '.',
                    'lap' => '^7' . $player['tb_string'],
                    'diff' => $player['tb_diff'] ? ' ' . MsgTypes::RED . $player['tb_diff'] : '',
                    'name' => "^8[{$player['car']}] {$player['name']}",
                    'splits' => implode('  |  ', $player['sectors_string']),
                );
            }

            $index++;
        }

        // Own time
        if ($ownTime) {
            $this->own_time = array(
                'n' => $ownTime['number'] . '.',
                'lap' => '^7' . $ownTime['tb_string'],
                'diff' => $player['tb_diff'] ? ' ' . MsgTypes::RED . $ownTime['tb_diff'] : '',
                'name' => "^8[{$ownTime['car']}] {$ownTime['name']}",
                'splits' => implode('  |  ', $ownTime['sectors_string']),
            );
        } else {
            $this->own_time = array(
                'n' => '',
                'lap' => '',
                'diff' => '',
                'name' => '',
                'splits' => '',
            );
        }

        $this->max_page = ceil(count($sc) / $this->rows_per_page);
        $this->status_line = $this->current_page . ' / ' . ($this->max_page > 0 ? $this->max_page : 1);
    }

    public function show($showBase = true) {
        $this->setData();

        if ($showBase)
            $this->showBase();

        $this->button_content_min = $this->id_current + 1;

        $this->showFooter();

        // Times
        $button = new isBTN();
        $newLine = 0;
        foreach ($this->rows as $key => $row) {
            $rowIndex = 0;
            foreach ($this->columns as $keyCol => $column) {
                $button->ReqI = ++$this->id_current;
                $button->ClickID = $button->ReqI;
                $button->L = !$rowIndex ? ($this->left + $this->content_left_offset) + 1 : $button->L + $button->W;
                $button->T = $this->top + 2 + 12 + $newLine;
                $button->H = 5;
                $button->W = $column['width'];
                $button->BStyle = ButtonStyles::ISB_CLICK + ButtonStyles::ISB_DARK + ButtonStyles::COLOUR_LIGHT_GREY + (isset($column['style']) ? $column['style'] : 0);
                $button->Text = isset($row[$keyCol]) ? $row[$keyCol] : '';

                $this->send($button);
                $this->rows_buttons[$key][$keyCol] = $button->ClickID;
                $rowIndex++;
            }
            $newLine += 5;
        }

        // Own time
        $newLine += 5;
        $rowIndex = 0;
        foreach ($this->columns as $keyCol => $column) {
            $button->ReqI = ++$this->id_current;
            $button->ClickID = $button->ReqI;
            $button->L = !$rowIndex ? ($this->left + $this->content_left_offset) + 1 : $button->L + $button->W;
            $button->T = $this->top + 2 + 12 + $newLine;
            $button->H = 5;
            $button->W = $column['width'];
            $button->BStyle = ButtonStyles::ISB_CLICK + ButtonStyles::ISB_DARK + ButtonStyles::COLOUR_LIGHT_GREY + (isset($column['style']) ? $column['style'] : 0);
            $button->Text = $this->own_time[$keyCol];

            $this->send($button);
            $this->own_buttons[$keyCol] = $button->ClickID;
            //$this->rows_buttons[$key][$keyCol] = $button->ClickID;
            $rowIndex++;
        }
        $newLine += 5;

        $this->button_content_max = $this->id_current;

        parent::show();
    }

    public function eventClick(isBTC $packet) {
        parent::eventClick($packet);

        // Lap time copy
        if ($packet->CFlags == isBTC::ISB_RMB) {
            foreach ($this->rows_buttons as $row_id => $buttons) {
                foreach ($buttons as $buttonID) {
                    if ($buttonID == $packet->ClickID) {
                        $this->actionCopyTime($this->rows[$row_id]);
                        return;
                    }
                }
            }
        }
    }

    public function actionCopyTime($data) {
        if (empty($data['lap'])) {
            return;
        }
        
        $text = "^7{$data['n']} {$data['name']}^8 - {$data['lap']} {$data['diff']}^8 (" . (str_replace('  |  ', ' | ', $data['splits'])) . ")";
        $msg = new isMTC();
        $msg->UCID = 255;
        $msg->Text = $text;
        $this->send($msg);
    }

}
