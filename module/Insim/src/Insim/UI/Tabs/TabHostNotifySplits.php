<?php

namespace Insim\UI\Tabs;

use Insim\Helper\InSimHelper;
use Insim\Model\PlayerClass;
use Insim\Types\MsgTypes;
use Insim\UI\Elements\BtnInput;
use Insim\UI\Elements\BtnLabel;
use Zend\Debug\Debug;

class TabHostNotifySplits extends Tab {
    protected $floatFilter = null;
            
    function __construct(PlayerClass &$player) {
        parent::__construct($player);
        
        $this->floatFilter = new \Insim\Filter\FloatFilter();
        
        $this->buttons = array(
            'labels' => new BtnLabel($player, '', $height = 5, $top = 0, $left = 0),
            'input_laps' => new BtnInput($player, 'Laps / splits % of WR', $height = 5, $top = 0, $left = 0),
        );
        
        $this->buttons['input_laps']->eventValueChanged = function($key, $value){
            $val = $this->floatFilter->filter($value) / 100;
            $this->player->host->settings->update($key, $val);
            
            return ($val * 100).'%';
        };
    }
    
    function setData($data = array()) {
        parent::setData($data);
        
        $lavelsRatings = array(MsgTypes::WHITE.'Good', MsgTypes::WHITE.'Great', MsgTypes::WHITE.'TOP', MsgTypes::WHITE.'WR');
        
        $values = array();
        $ratings = ['ntf_lap_good', 'ntf_lap_great', 'ntf_lap_top', 'ntf_lap_wr'];
        foreach($ratings as $rating){
            $ratio = floatval($this->player->host->settings->get($rating)) * 100;
            $values[$rating] = MsgTypes::WHITE."{$ratio}%";
        }
        
        // Debug calc times from XFG BL1 WR
        /*$wr = [92700,29650,66360];
        foreach ($wr as $time){
            foreach($ratings as $rating){
                $target = ($this->player->host->settings->get($rating) + 1) * $time;
                Debug::dump(array(
                    'WR '.$rating => InSimHelper::timeToString($time),
                    'target' => InSimHelper::timeToString($target)
                ));
            }
        }*/
        
        $this->buttons['labels']->setValues($lavelsRatings);
        $this->buttons['input_laps']->setValues($values);
    }
}
