<?php

namespace Insim\UI\Tabs;

use Insim\Model\PlayerClass;
use Insim\Types\FalseStartAction;
use Insim\Types\GridModes;
use Insim\Types\MsgTypes;
use Insim\UI\Elements\BtnInput;
use Insim\UI\Elements\BtnSwitch;

class TabHostRaceGrid extends Tab {

    function __construct(PlayerClass &$player) {
        parent::__construct($player);

        $this->buttons = array(
            'race-grid-mode' => new BtnSwitch($player, 'Start order:', 5),
            'race-grid-reverse' => new BtnInput($player, 'Reverse top (X or X% or 0 = all) cars:', 5),
            'race-false-start' => new BtnSwitch($player, 'False start action:', 5, 5),
        );

        // Events
        $this->buttons['race-grid-mode']->eventValueChanged = function($value, $key) {
            $key = substr($key, 2);
            $this->player->host->settings->update('race-grid-mode', $value);
        };

        $this->buttons['race-grid-reverse']->eventValueChanged = function($key, $value) {
            $percent = false;
            if (strpos($value, '%') !== FALSE) {
                $percent = true;
            }

            $val = intval($value);

            if ($val == 0) {
                $this->player->host->settings->update('race-grid-reverse', $val);
                return 'all';
            }

            $val = $val < 1 ? 1 : $val;
            $val = $val . ($percent ? '%' : '');

            $this->player->host->settings->update('race-grid-reverse', $val);
            return $val;
        };

        $this->buttons['race-false-start']->eventValueChanged = function($value, $key) {
            $key = substr($key, 2);
            $this->player->host->settings->update('race-false-start', $value);
        };
    }

    function setData($data = array()) {
        parent::setData($data);

        $valuesGridMode = array(
            GridModes::FINISH => MsgTypes::GREEN . 'Finish',
            GridModes::REVERSE => MsgTypes::GREEN . 'Reverse',
            GridModes::RANDOM => MsgTypes::GREEN . 'Random',
        );

        $valuesFalseStart = array(
            FalseStartAction::PENALTY => MsgTypes::GREEN . 'Penalty',
            FalseStartAction::PIT => MsgTypes::GREEN . 'Pit',
        );

        $gridReverse = $this->player->host->settings->get('race-grid-reverse') == 0 ? 'all' : $this->player->host->settings->get('race-grid-reverse');

        $this->buttons['race-grid-mode']->setValues($valuesGridMode, $this->player->host->settings->get('race-grid-mode'));
        $this->buttons['race-grid-reverse']->setValues(array('race-grid-reverse' => MsgTypes::WHITE . $gridReverse));
        $this->buttons['race-false-start']->setValues($valuesFalseStart, $this->player->host->settings->get('race-false-start'));
    }

}
