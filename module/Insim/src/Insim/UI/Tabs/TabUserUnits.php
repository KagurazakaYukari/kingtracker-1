<?php

namespace Insim\UI\Tabs;

use Insim\Model\PlayerClass;
use Insim\Types\MsgTypes;
use Insim\Types\SpeedUnit;
use Insim\UI\Elements\BtnSwitch;

class TabUserUnits extends Tab {

    function __construct(PlayerClass &$player) {
        parent::__construct($player);

        $this->buttons = array(
            'unit_speed' => new BtnSwitch($player, $this->player->translator->translateLFS('SELECT_SPEED_UNIT'), 5),
        );

        $this->buttons['unit_speed']->eventValueChanged = function($value) {
            $this->player->settings->update('unit_speed', $value);
        };
    }

    function setData($data = array()) {
        parent::setData($data);

        $valuesYesNo = array(
            SpeedUnit::KPH => MsgTypes::GREEN . $this->player->translator->translateLFS('KPH'),
            SpeedUnit::MPH => MsgTypes::GREEN . $this->player->translator->translateLFS('MPH'),
        );

        $this->buttons['unit_speed']->setValues($valuesYesNo, @$this->player->settings->get('unit_speed'));
    }

}
