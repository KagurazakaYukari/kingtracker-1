<?php

namespace Insim\UI\Tabs;

use Insim\Model\PlayerClass;
use Insim\Types\MsgTypes;
use Insim\Types\NotifyTypes;
use Insim\UI\Elements\BtnSwitch;

class TabUserNotifyGeneral extends Tab {

    function __construct(PlayerClass &$player) {
        parent::__construct($player);

        $this->buttons = array(
            'ntf_hlvc' => new BtnSwitch($player, $this->player->translator->translateLFS('SHOW_HLVC'), 5),
            'ntf_split_good' => new BtnSwitch($player, $this->player->translator->translateLFS('SHOW_SPLIT_GOOD'), 5),
            'ntf_lap_good' => new BtnSwitch($player, $this->player->translator->translateLFS('SHOW_LAP_GOOD'), 5),
            'ntf_split_great' => new BtnSwitch($player, $this->player->translator->translateLFS('SHOW_SPLIT_GREAT'), 5),
            'ntf_lap_great' => new BtnSwitch($player, $this->player->translator->translateLFS('SHOW_LAP_GREAT'), 5),
            'ntf_split_top' => new BtnSwitch($player, $this->player->translator->translateLFS('SHOW_SPLIT_TOP'), 5),
            'ntf_lap_top' => new BtnSwitch($player, $this->player->translator->translateLFS('SHOW_LAP_TOP'), 5),
            'ntf_split_wr' => new BtnSwitch($player, $this->player->translator->translateLFS('SHOW_SPLIT_WR'), 5),
            'ntf_lap_wr' => new BtnSwitch($player, $this->player->translator->translateLFS('SHOW_LAP_WR'), 5),
        );

        $this->buttons['ntf_hlvc']->eventValueChanged = function($value) {
            $this->player->settings->update('ntf_hlvc', $value);
        };
        $this->buttons['ntf_split_good']->eventValueChanged = function($value) {
            $this->player->settings->update('ntf_split_good', $value);
        };
        $this->buttons['ntf_lap_good']->eventValueChanged = function($value) {
            $this->player->settings->update('ntf_lap_good', $value);
        };
        $this->buttons['ntf_split_great']->eventValueChanged = function($value) {
            $this->player->settings->update('ntf_split_great', $value);
        };
        $this->buttons['ntf_lap_great']->eventValueChanged = function($value) {
            $this->player->settings->update('ntf_lap_great', $value);
        };
        $this->buttons['ntf_split_top']->eventValueChanged = function($value) {
            $this->player->settings->update('ntf_split_top', $value);
        };
        $this->buttons['ntf_lap_top']->eventValueChanged = function($value) {
            $this->player->settings->update('ntf_lap_top', $value);
        };
        $this->buttons['ntf_split_wr']->eventValueChanged = function($value) {
            $this->player->settings->update('ntf_split_wr', $value);
        };
        $this->buttons['ntf_lap_wr']->eventValueChanged = function($value) {
            $this->player->settings->update('ntf_lap_wr', $value);
        };
    }

    function setData($data = array()) {
        parent::setData($data);

        $valuesYesNo = array(
            NotifyTypes::NO => MsgTypes::RED . $this->player->translator->translateLFS('NO'),
            NotifyTypes::YES => MsgTypes::GREEN . $this->player->translator->translateLFS('YES'),
        );

        $valuesYesNoOwn = array(
            NotifyTypes::NO => MsgTypes::RED . $this->player->translator->translateLFS('NO'),
            NotifyTypes::YES => MsgTypes::GREEN . $this->player->translator->translateLFS('YES'),
            NotifyTypes::OWN => MsgTypes::GREEN . $this->player->translator->translateLFS('OWN'),
        );

        $this->buttons['ntf_hlvc']->setValues($valuesYesNo, @$this->player->settings->get('ntf_hlvc'));
        $this->buttons['ntf_split_good']->setValues($valuesYesNoOwn, @$this->player->settings->get('ntf_split_good'));
        $this->buttons['ntf_lap_good']->setValues($valuesYesNoOwn, @$this->player->settings->get('ntf_lap_good'));
        $this->buttons['ntf_split_great']->setValues($valuesYesNoOwn, @$this->player->settings->get('ntf_split_great'));
        $this->buttons['ntf_lap_great']->setValues($valuesYesNoOwn, @$this->player->settings->get('ntf_lap_great'));
        $this->buttons['ntf_split_top']->setValues($valuesYesNoOwn, @$this->player->settings->get('ntf_split_top'));
        $this->buttons['ntf_lap_top']->setValues($valuesYesNoOwn, @$this->player->settings->get('ntf_lap_top'));
        $this->buttons['ntf_split_wr']->setValues($valuesYesNoOwn, @$this->player->settings->get('ntf_split_wr'));
        $this->buttons['ntf_lap_wr']->setValues($valuesYesNoOwn, @$this->player->settings->get('ntf_lap_wr'));
    }

}
