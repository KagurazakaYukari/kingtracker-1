<?php

namespace Insim\UI\Tabs;

use Insim\Model\PlayerClass;

class TabHostRaceManagement extends Tab {

    function __construct(PlayerClass &$player) {
        parent::__construct($player);
                        
        $this->buttons = array();
    }
    
    function setData($data = array()) {
        parent::setData($data);
    }

}
