<?php

namespace Insim\UI;

use Insim\Model\PlayerClass;
use Insim\Packets\isBTC;
use Insim\Packets\isBTN;
use Insim\Packets\isBTT;
use Insim\Types\ButtonStyles;

class UILayouts extends UI {

    public $columns = array();
    public $rows = array();
    public $cars = array();
    public $filter = array();
    public $search = array();
    public $topActions = array();
    public $own_time = array();
    public $own_buttons = array();
    public $rows_buttons = array();
    public $rows_per_page = 17;
    public $items_per_page = 17;
    public $show_help = false;
    public $trackCode;

    function __construct($alias, PlayerClass &$player, $width = 72, $height = 102, $top = 30) {
        $this->width = $width;
        $this->height = $height;
        $this->top = $top;
        $this->left = (200 - $width) / 2;

        // columns
        $this->columns = array(
            'layout' => array('width' => 55, 'name' => 'layout', 'style' => ButtonStyles::ISB_LEFT),
            'load' => array('width' => 15, 'name' => 'load', 'style' => 0),
        );

        // Top actions
        $this->topActions = array(
            'track' => array('id' => 0, 'state' => true, 'title' => $player->translator->translateLFS('LAYOUT_TRACK') . " {$player->host->TrackInfo->name}", 'width' => 15, 'style' => ButtonStyles::ISB_LEFT),
            'clear' => array('id' => 0, 'state' => true, 'title' => $player->translator->translateLFS('LAYOUT_CLEAR'), 'width' => 15, 'callback' => 'actionClearLayout', 'style' => ButtonStyles::ISB_LEFT),
        );

        parent::__construct($alias, $player);
    }

    public function setData() {
        $this->rows = array();

        $index = 0;
        $start = $this->current_page == 1 ? 0 : ($this->current_page - 1) * $this->rows_per_page;
        $end = $start + $this->rows_per_page - 1;

        $layouts = $this->player->host->layoutService->getAvailableLayouts($this->trackCode);

        foreach ($layouts as $layout) {
            if ($index >= $start && $index <= $end) {
                $this->rows[] = array(
                    'layout' => \Insim\Types\MsgTypes::WHITE . $layout,
                    'load' => ($this->player->host->LName == (isset($this->player->host->TrackInfo->name) ? "{$this->player->host->TrackInfo->name}_{$layout}" : '')) ? 1 : 0
                );
            }

            $index++;
        }

        for ($i = count($this->rows); $i < $this->rows_per_page; $i++) {
            $this->rows[$i] = array('layout' => '', 'load' => -1);
        }

        $this->max_page = floor(count($this->player->commandService->commands) / $this->rows_per_page);
        $this->status_line = $this->current_page . ' / ' . $this->max_page;
    }

    public function show($showBase = true) {
        $this->setData();

        if ($showBase)
            $this->showBase();

        $this->button_content_min = $this->id_current + 1;

        $this->showTopActions();

        $button = new isBTN();
        $newLine = 0;
        foreach ($this->rows as $key => $row) {
            $rowIndex = 0;
            foreach ($this->columns as $keyCol => $column) {
                $button->ReqI = ++$this->id_current;
                $button->ClickID = $button->ReqI;
                $button->L = !$rowIndex ? ($this->left) + 1 : $button->L + $button->W;
                $button->T = $this->top + 2 + 12 + $newLine;
                $button->H = 5;
                $button->W = $column['width'];
                $button->BStyle = ButtonStyles::ISB_CLICK + ButtonStyles::ISB_DARK + ButtonStyles::COLOUR_LIGHT_GREY + (isset($column['style']) ? $column['style'] : 0);


                if ($keyCol == 'load') {
                    $value = isset($row[$keyCol]) ? $row[$keyCol] : '';

                    switch ($value) {
                        case 0: $value = \Insim\Types\MsgTypes::YELLOW . $this->player->translator->translateLFS('LOAD');
                            break;
                        case 1: $value = \Insim\Types\MsgTypes::GREEN . $this->player->translator->translateLFS('LOADED');
                            break;
                        case -1: $value = '';
                            break;
                    }

                    //$this->player->translator->translateLFS('LAYOUTS_TITLE');
                    $button->Text = $value;
                } else {
                    $button->Text = isset($row[$keyCol]) ? $row[$keyCol] : '';
                }

                $this->send($button);
                $this->rows_buttons[$key][$keyCol] = $button->ClickID;
                $rowIndex++;
            }
            $newLine += 5;
        }


        $this->showFooter(-2);


        $this->button_content_max = $this->id_current;

        parent::show();
    }

    public function showTopActions() {
        $button = new isBTN();

        $left = $this->left + 3;
        foreach ($this->topActions as $key => $action) {
            $style = isset($action['style']) ? $action['style'] : 0;

            $button->ReqI = ++$this->id_current;
            $button->ClickID = $button->ReqI;
            $button->L = $left;
            $button->T = $this->top + 2 + 6;
            $button->H = 4;
            $button->W = $action['width'];
            $button->BStyle = ButtonStyles::COLOUR_LIGHT_GREY + ButtonStyles::ISB_CLICK + $style;
            $button->Text = $action['title'];
            if (isset($action['typein'])) {
                $button->TypeIn = $action['typein'];
            } else {
                $button->TypeIn = 0;
            }

            $this->send($button);

            $left += $button->W + 1;
            $this->topActions[$key]['id'] = $button->ClickID;
        }
    }

    public function redrawContent() {
        $this->setData();

        $button = new isBTN();
        $button->ClickID = $this->button_id_status_line;
        $button->Text = $this->status_line;
        $this->send($button);

        foreach ($this->rows as $row_id => $row) {
            $buttons = $this->rows_buttons[$row_id];

            foreach ($buttons as $key => $id) {
                $button->ClickID = $id;

                if ($key == 'load') {
                    $value = isset($row[$key]) ? $row[$key] : '';

                    switch ($value) {
                        case 0: $value = \Insim\Types\MsgTypes::YELLOW . $this->player->translator->translateLFS('LOAD');
                            break;
                        case 1: $value = \Insim\Types\MsgTypes::GREEN . $this->player->translator->translateLFS('LOADED');
                            break;
                        case -1: $value = '';
                            break;
                    }

                    //$this->player->translator->translateLFS('LAYOUTS_TITLE');
                    $button->Text = $value;
                } else {
                    $button->Text = isset($row[$key]) ? $row[$key] : '';
                }

                //$button->Text = $this->rows[$row_id][$key];
                $this->send($button);
            }
        }

        parent::redrawContent();
    }

    public function eventClick(isBTC $packet) {
        // top action click
        foreach ($this->topActions as $key => $action) {
            if ($action['id'] == $packet->ClickID && !isset($action['typein'])) {
                if (isset($action['callback']))
                    $this->{$action['callback']}();
                return;
            }
        }

        foreach ($this->rows_buttons as $row_id => $buttons) {
            foreach ($buttons as $keyCol => $buttonID) {
                if ($buttonID == $packet->ClickID) {
                    if (!isset($this->rows[$row_id]['layout']) OR $this->rows[$row_id]['load'] == 1)
                        continue;

                    if ($keyCol == 'load') {
                        $this->actionLoadLayout($this->rows[$row_id]['layout']);
                    }
                }
            }
        }

        parent::eventClick($packet);
    }

    public function eventType(isBTT $packet) {
        parent::eventType($packet);
    }

    public function actionClearLayout() {
        $this->player->host->layoutService->clearLayout(function() {
            $this->redrawContent();
        });
    }

    public function actionLoadLayout($name) {
        if ($name) {
            $this->player->host->layoutService->changeLayout($name, function() {
                $this->redrawContent();
            });
        }
    }

}
