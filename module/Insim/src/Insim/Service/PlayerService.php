<?php

namespace Insim\Service;

use Application\Service\CoreService;
use DateTime;
use DateTimeZone;
use Exception;
use Insim\Helper\Math;
use Insim\Model\Host;
use Insim\Model\Player;
use Insim\Model\PlayerACL;
use Insim\Model\PlayerBan;
use Insim\Model\PlayerClass;
use Insim\Model\PlayerRecent;
use Insim\Model\RaceStats;
use Insim\Packets\isCCH;
use Insim\Packets\isCNL;
use Insim\Packets\isCON;
use Insim\Packets\isCPR;
use Insim\Packets\isCRS;
use Insim\Packets\isFIN;
use Insim\Packets\isHLV;
use Insim\Packets\isLap;
use Insim\Packets\isMCI;
use Insim\Packets\isNCI;
use Insim\Packets\isNCN;
use Insim\Packets\isNPL;
use Insim\Packets\isOBH;
use Insim\Packets\isPEN;
use Insim\Packets\isPFL;
use Insim\Packets\isPIT;
use Insim\Packets\isPLA;
use Insim\Packets\isPLL;
use Insim\Packets\isPSF;
use Insim\Packets\isSPX;
use Insim\Packets\Packet;
use Insim\Types\FalseStartAction;
use Insim\Types\MsgTypes;
use Insim\Types\PenaltyReason;
use Insim\Types\PitWork;
use Insim\Types\PlayerFlags;
use LfsString;
use Zend\Db\Sql\Predicate\Expression;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Where;
use Zend\Db\TableGateway\TableGateway;
use Zend\Debug\Debug;

require_once(VENDOR_PATH . '/lfs/LFSString.php');

class PlayerService extends CoreService {

    public $host;
    public $localCFG;
    public $players = array();
    public $tablePlayers = null;
    public $tablePlayersBan = null;
    public $tablePlayersRecent = null;
    public $ACLService;
    public $UIService;
    public $lapsService;
    public $timecompareService;
    public $commandService;
    public $playerSttingsService;
    public $raceStats;
    public $translator;

    public function __construct(TableGateway $tableGateway) {
        parent::__construct($tableGateway);
        
        $this->debug = true;
    }

    public function setHost(Host &$host){
        $this->host = $host;
        
        $this->raceStats = new RaceStats();
        $this->raceStats->host = $host;
    }

    public function handlePacket($header, $rawPacket) {
        switch ($header['Type']) {
            case Packet::ISP_NCN:
                $this->newConnection($rawPacket);
                break;
            case Packet::ISP_NCI:
                $this->newConnectionInfo($rawPacket);
                break;
            case Packet::ISP_CNL:
                $this->connectionLeave($rawPacket);
                break;
            case Packet::ISP_NPL:
                $this->newPlayer($rawPacket);
                break;
            case Packet::ISP_CPR:
                $this->connectionRename($rawPacket);
                break;
            case Packet::ISP_PLL:
                $this->playerLeave($rawPacket);
                break;
            case Packet::ISP_PFL:
                $this->playerChangeFlags($rawPacket);
                break;
            case Packet::ISP_LAP:
                $this->lapPacket($rawPacket);
                break;
            case Packet::ISP_SPX:
                $this->splitPacket($rawPacket);
                break;
            case Packet::ISP_MCI:
                $this->carPositionPacket($rawPacket);
                break;
            case Packet::ISP_HLV:
                $this->hlvcPacket($rawPacket);
                break;
            case Packet::ISP_CON:
                $this->carContactCar($rawPacket);
                break;
            case Packet::ISP_CRS:
                $this->carReset($rawPacket);
                break;
            case Packet::ISP_OBH:
                $this->objContact($rawPacket);
                break;
            case Packet::ISP_FIN:
                $this->playerFinished(new isFIN($rawPacket));
                break;
            case Packet::ISP_PLA:
                $this->playerPitLane(new isPLA($rawPacket));
                break;
            case Packet::ISP_PIT:
                $this->playerPitStop(new isPIT($rawPacket));
                break;
            case Packet::ISP_PSF:
                $this->playerPitFinished(new isPSF($rawPacket));
                break;
            case Packet::ISP_PEN:
                $this->playerPenalty(new isPEN($rawPacket));
            case Packet::ISP_CCH:
                $this->cameraChange(new isCCH($rawPacket));
        }

        // Update all players on host
        if (!isset($this->players[$this->host->id]))
            return;

        if ($this->host->local) {
            foreach ($this->players[$this->host->id] as $ucid => $player) {
                
                if ($player->local) {
                    $player->ui->handlePacket($header, $rawPacket);
                    $player->commandService->handlePacket($header, $rawPacket);
                }
            }
        } else {
            foreach ($this->players[$this->host->id] as $ucid => $player) {
                $player->ui->handlePacket($header, $rawPacket);
                $player->commandService->handlePacket($header, $rawPacket);
            }
        }
    }

    public function update() {
        if (isset($this->players[$this->host->id])) {
            foreach ($this->players[$this->host->id] as $player) {
                $player->ui->update();
            }
        }
    }

    // Packet handlers
    public function newConnection($rawPacket) {
        $packet = new isNCN($rawPacket);

        // Skip host connection
        if ($packet->UCID == 0) {
            return;
        }

        $this->saveConn($packet);

        // TODO: total connection on server
        //$packet->Total;

        $this->log('New connection');
    }

    public function newConnectionInfo($rawPacket) {
        $packet = new isNCI($rawPacket);

        $this->saveConnInfo($packet);

        $this->log('New connection info');
    }

    public function connectionLeave($rawPacket) {
        $packet = new isCNL($rawPacket);

        $this->deleteConn($packet);

        $this->log('Connection leave');
    }

    public function connectionRename($rawPacket) {
        $packet = new isCPR($rawPacket);

        $where = array('UCID' => $packet->UCID, 'host_id' => $this->host->id);

        $conn = $this->getBy($where, $this->tableConns);

        if ($conn) {
            $conn->PName = $packet->PName;
            $conn->PName_utf = LfsString::convertWithoutColor($packet->PName);

            $data = $conn->exchangeObject();
            $this->tableConns->update($data, $where);
        }

        // Players DB
        $playerDB = $this->getBy(array('UName' => $conn->UName), $this->tablePlayers);
        if ($playerDB) {
            $playerDB->PName = $conn->PName;
            $playerDB->PName_utf = $conn->PName_utf;
            $this->tablePlayers->update($playerDB->exchangeObject(), array('UName' => $conn->UName));
        }

        // Run-time players list update
        if (isset($this->players[$this->host->id][$packet->UCID])) {
            $this->players[$this->host->id][$packet->UCID]->name = $packet->PName;
        }

        // Update open pl list
        foreach ($this->players[$this->host->id] as $player) {
            if (isset($player->ui->ui_list['playerList']) && $player->ui->ui_list['playerList']->displayed) {
                $player->ui->ui_list['playerList']->updatePlayerName($conn);
            }
        }
    }

    public function newPlayer($rawPacket) {
        $packet = new isNPL($rawPacket);
        $this->log('New player: ' . $packet->PLID);

        $data = $packet->exchangeObject();
        $data['host_id'] = $this->host->id;
        $data['PName_utf'] = LfsString::convertWithoutColor($packet->PName);
        $data['Flags_string'] = PlayerFlags::toString($packet->Flags);

        $where = array('UCID' => $packet->UCID, 'host_id' => $this->host->id);
        $pll = $this->getBy($where, $this->tableGateway);

        if ($pll) {
            $this->tableGateway->update($data, $where);
        } else {
            $this->tableGateway->insert($data);
        }

        //Debug::dump($packet);
        
        // UI
        // Run-time players list update
        if (isset($this->players[$this->host->id][$packet->UCID])) {
            $this->players[$this->host->id][$packet->UCID]->name = $packet->PName;
            $this->players[$this->host->id][$packet->UCID]->PLID = $packet->PLID;
            $this->players[$this->host->id][$packet->UCID]->pll = $packet;
            $this->players[$this->host->id][$packet->UCID]->timecompareService->newPlayerEvent();
            $this->players[$this->host->id][$packet->UCID]->lapsService->newPlayerEvent();
            
            // Events
            $this->eventPlayerJoinedTrack($this->players[$this->host->id][$packet->UCID]);
            $this->players[$this->host->id][$packet->UCID]->ui->eventJoinTrack();
            $this->raceStats->eventNewPlayer($this->players[$this->host->id][$packet->UCID]);

            // Update all open PL lists
            foreach ($this->players[$this->host->id] as $player) {
                if (isset($player->ui->ui_list['playerList']) && $player->ui->ui_list['playerList']->displayed) {
                    $player->ui->ui_list['playerList']->updatePlayersCar($data);
                }
            }

            // Update top laps car filter
            foreach ($this->players[$this->host->id] as $player) {
                if (isset($player->ui->ui_list['topLaps']) && $player->ui->ui_list['topLaps']->displayed && $player->ui->player->PLID == $packet->PLID) {
                    $player->ui->ui_list['topLaps']->setCarFilter($packet->CName);
                }
            }
        }
    }

    public function playerLeave($rawPacket) {
        $packet = new isPLL($rawPacket);

        $where = new Where();
        $where->equalTo('PLID', $packet->PLID);
        $where->equalTo('host_id', $this->host->id);

        $this->tableGateway->delete($where);

        // Update runtime players list
        foreach ($this->players[$this->host->id] as $UCID => $player) {
            if ($player->PLID == $packet->PLID) {
                $this->players[$this->host->id][$UCID]->PLID = 0;
                $this->players[$this->host->id][$UCID]->pll = null;
                $this->players[$this->host->id][$UCID]->lapsService->playerLeaveEvent();

                $this->players[$this->host->id][$UCID]->ui->eventSpetate();
            }

            // Update UI
            if (isset($player->ui->ui_list['playerList']) && $player->ui->ui_list['playerList']->displayed) {
                $player->ui->ui_list['playerList']->updatePlayersCarRemove($packet->PLID);
            }
        }
    }

    public function playerChangeFlags($rawPacket) {
        $packet = new isPFL($rawPacket);

        $where = array('PLID' => $packet->PLID, 'host_id' => $this->host->id);
        $pll = $this->getBy($where, $this->tableGateway);

        if ($pll) {
            $pll->Flags = $packet->Flags;
            $pll->Flags_string = PlayerFlags::toString($packet->Flags);
            $data = $pll->exchangeObject();
            $this->tableGateway->update($data, $where);
        }

        foreach ($this->players[$this->host->id] as $UCID => $player) {
            if ($player->PLID == $packet->PLID) {
                $this->players[$this->host->id][$UCID]->pll->Flags = $packet->Flags;
            }

            // Update all open PL lists
            if (isset($player->ui->ui_list['playerList']) && $player->ui->ui_list['playerList']->displayed) {
                $player->ui->ui_list['playerList']->updatePlayersCar($data);
            }
        }
    }

    public function findPlayer($host_id, $UCID) {
        return isset($this->players[$host_id][$UCID]) ? $this->players[$host_id][$UCID] : false;
    }

    public function findPlayerByPLID($host_id, $PLID) {
        if (!isset($this->players[$host_id]))
            return;

        foreach ($this->players[$host_id] as $player) {
            if ($player->PLID == $PLID) {
                return $player;
            }
        }
    }

    public function removePlayer($host_id, isCNL $packet) {
        $UCID = $packet->UCID;

        if (!isset($this->players[$host_id])) {
            return;
        }

        // Recent players list
        if (isset($this->players[$host_id][$UCID])) {
            $recent = new PlayerRecent();
            $recent->player_id = $this->players[$host_id][$UCID]->player_id;
            $recent->host_id = $host_id;
            $recent->reason = $packet->Reason;
            $recent->PName = $this->players[$host_id][$UCID]->name;
            $recent->UName = $this->players[$host_id][$UCID]->UName;

            $this->save($recent, $this->tablePlayersRecent);

            unset($this->players[$host_id][$UCID]);
        }

        foreach ($this->players[$host_id] as $player) {
            if (isset($player->ui->ui_list['playerList']) && $player->ui->ui_list['playerList']->displayed) {
                $player->ui->updatePlayersListRemove($UCID);
            }
        }
    }

    public function lapPacket($rawPacket) {
        $lap = new isLap($rawPacket);

        if (!isset($this->players[$this->host->id])) {
            return;
        }

        $pitPenalty = $this->host->settings->get('race-pit-penalty');

        foreach ($this->players[$this->host->id] as $ucid => $player) {
            if ($lap->PLID == $player->PLID) {
                $player->lapsService->lapEvent($lap);
                $player->ui->eventLap($lap);
                $this->raceStats->eventLap($lap);

                // Check required pit stops missed windows
                if (!$this->checkRequiredPitWindows($lap->LapsDone, $player, $pitNumber)) {
                    $this->host->sendMsgHost("{$player->name} ^8missed required pit stop window");

                    if ($pitPenalty != 'none') {
                        $this->host->givePlayerPenalty($player->UName, strtolower($pitPenalty));
                    }

                    if (isset($pitNumber)) {
                        $player->pitstops[$pitNumber] = true;
                    }
                }
            }
        }
    }

    public function splitPacket($rawPacket) {
        $split = new isSPX($rawPacket);
        if (!isset($this->players[$this->host->id])) {
            return;
        }

        foreach ($this->players[$this->host->id] as $ucid => $player) {
            if ($split->PLID == $player->PLID) {
                $player->lapsService->splitEvent($split);
                $player->ui->eventSplit($split);
                $this->raceStats->eventSplit($split);
            }
        }
    }

    public function carContactCar($rawPacket) {
        $con = new isCON($rawPacket);

        if (!isset($this->players[$this->host->id])) {
            return;
        }

        // Find cars involved
        foreach ($this->players[$this->host->id] as $ucid => $player) {
            if ($con->A->PLID == $player->PLID) {
                $player->lapsService->contactCarEvent($con);
            }
            if ($con->B->PLID == $player->PLID) {
                $player->lapsService->contactCarEvent($con);
            }
        }
    }

    public function carPositionPacket($rawPacket) {
        $mci = new isMCI($rawPacket);

        if (!isset($this->players[$this->host->id])) {
            return;
        }

        if ($this->host->local) {
            return;
        }

        if ($mci->NumC < 1) {
            return;
        }

        // Set car position packet to each player
        foreach ($mci->Info as $compCar) {
            $player = $this->findPlayerByPLID($this->host->id, $compCar->PLID);
            if ($player) {
                $player->setCompCar($compCar);
            }
        }

        // Idle detection
        $this->host->safetyService->eventCarPositionUpdated();

        // Slipstream detection
        foreach ($this->players[$this->host->id] as $carTest) {
            // Car is on track
            if ($carTest->PLID && $carTest->compCar) {
                $debug = array();

                foreach ($this->players[$this->host->id] as $player) {
                    // Is not tested, car is on track and not laggging 
                    if ($player->PLID && $player->compCar && $player->PLID != $carTest->PLID && !$player->compCar->isLagging()) {

                        $return = array();
                        if (Math::isSlipStream($carTest->position, $player->position, $carTest->compCar, $player->UName, $return)) {
                            $carTest->lapsService->draftEvent();
                        }

                        if (!empty($return)) {
                            $return['name'] = $player->name;
                            $debug[] = $return;
                        }
                    }
                }
            }
        }
    }

    public function carPositionLocal() {
        $me = $this->host->ViewPLID;

        $carTest = false;

        if (!isset($this->players[$this->host->id])) {
            return;
        }

        foreach ($this->players[$this->host->id] as $player) {
            if ($player->PLID == $me && $player->compCar) {
                $carTest = $player;
            }
            //echo $player->UName.'- '.$player->PLID.PHP_EOL;
        }

        $debug = array();

        foreach ($this->players[$this->host->id] as $player) {
            if ($carTest && $carTest->compCar && $player->PLID && $player->compCar && $player->PLID != $me) {
                if ($player->compCar)
                    $return = array();

                if (Math::isSlipStream($carTest->position, $player->position, $carTest->compCar, $player->UName, $return)) {
                    // Slip stream detected
                    $carTest->lapsService->draftEvent();
                }

                if (!empty($return)) {
                    $return['name'] = $player->name;
                    $debug[] = $return;
                }
            }
        }

        // Update debug UI
        if (isset($this->players[$this->host->id])) {
            foreach ($this->players[$this->host->id] as $player) {
                if (isset($player->ui->ui_list['debug_lap']) && $player->ui->ui_list['debug_lap']->displayed) {
                    $player->ui->ui_list['debug_lap']->update($debug);
                }
            }
        }
    }

    public function hlvcPacket($rawPacket) {
        $hlvc = new isHLV($rawPacket);

        if (!isset($this->players[$this->host->id])) {
            return;
        }

        foreach ($this->players[$this->host->id] as $ucid => $player) {
            if ($hlvc->PLID == $player->PLID) {
                $player->lapsService->hlvcEvent($hlvc);
            }
        }
    }

    public function carReset($rawPacket) {
        $crs = new isCRS($rawPacket);

        foreach ($this->players[$this->host->id] as $ucid => $player) {
            if ($crs->PLID == $player->PLID) {
                $player->lapsService->carResetEvent($crs);
                $player->ui->eventCarReset();
            }
        }
    }

    public function objContact($rawPacket) {
        $obh = new isOBH($rawPacket);

        foreach ($this->players[$this->host->id] as $ucid => $player) {
            if ($obh->PLID == $player->PLID) {
                $player->lapsService->objectContactEvent($obh);
            }
        }
    }

    public function playerFinished(isFIN $packet) {
        $this->host->hostService->raceFinished($packet);

        // Check required pitstops
        $pitPenalty = $this->host->settings->get('race-pit-penalty');
        $pits = (int) $this->host->settings->get('race-pits');
        if ($pits > 1) {
            foreach ($this->players[$this->host->id] as $ucid => $player) {
                if ($packet->PLID == $player->PLID) {
                    $pitsDone = 0;

                    foreach ($player->pitstops as $done) {
                        if ($done) {
                            $pitsDone++;
                        }
                    }

                    if ($pitsDone > 0 && $pitsDone < $pits) {
                        $this->host->sendMsgHost("{$player->name} ^8 did not made all required pit stops ({$pitsDone}/{$pits})");

                        if ($pitPenalty != 'none') {
                            $this->host->givePlayerPenalty($player->UName, strtolower($pitPenalty));
                        }
                    }
                }
            }
        }
    }

    /**
     * Player penalty packet
     * @param isPEN $packet
     */
    public function playerPenalty(isPEN $packet) {
        switch ($packet->Reason) {
            case PenaltyReason::PENR_FALSE_START:
                $this->eventPlayerFalseStart($packet);
                break;
        }

        //Debug::dump($packet);
    }
    
    public function cameraChange(isCCH $packet){
        //Debug::dump($packet);
    }

    // Required pit stops
    /**
     * Called when player pit stops
     * @param isPIT $packet
     * @return type
     */
    public function playerPitStop(isPIT $packet) {
        $pits = (int) $this->host->settings->get('race-pits');

        // Required pits check
        if ($pits == 0)
            return;

        $currentLap = $packet->LapsDone + 1;
        $UCID = null;
        $donePits = array();

        // Get players done pitstops
        foreach ($this->players[$this->host->id] as $ucid => $player) {
            if ($packet->PLID == $player->PLID) {
                $donePits = $player->pitstops;
                $UCID = $player->UCID;
            }
        }

        // Find required pit number
        $pitNumber = $this->host->settings->getRequiredPitNumber($currentLap, $donePits);


        // Find required work
        $requiredWork = $this->host->settings->getPitWork($pitNumber);


        if ($pitNumber == 0) {
            $this->host->sendMsgPlayer("Pit stop is not in window", $UCID, MsgTypes::YELLOW);
        } else {
            // Required work check
            $workDone = false;

            switch ($requiredWork) {
                case 'fuel':
                    $workDone = PitWork::isRefuel($packet->Work) ? true : false;
                    break;
                case 'tyres':
                    $workDone = PitWork::isAllTyres($packet->Work) ? true : false;
                    break;
                case 'tyres|fuel':
                    $refule = PitWork::isRefuel($packet->Work) ? true : false;
                    $tyres = PitWork::isAllTyres($packet->Work) ? true : false;
                    $workDone = $refule OR $tyres;
                    break;
                default:
                    $workDone = true;
                    break;
            }

            foreach ($this->players[$this->host->id] as $ucid => $player) {
                if ($packet->PLID == $player->PLID) {

                    // Count this pit
                    if (!empty($pitNumber)) {
                        if ($workDone) {
                            $player->pitstops[$pitNumber] = true;

                            $count_done = 0;
                            foreach ($player->pitstops as $pt) {
                                if ($pt) {
                                    $count_done++;
                                }
                            }

                            $this->host->sendMsgPlayer("Required pit stops done {$count_done}/{$pits}", $UCID, MsgTypes::YELLOW);
                        } else {
                            $this->host->sendMsgPlayer("Required work ({$requiredWork}) wasn't done", $UCID, MsgTypes::YELLOW);
                        }
                    }

                    //Debug::dump($player->pitstops);
                }
            }
        }

        /* Debug::dump([
          'lap' => $currentLap,
          'pit' => $pitNumber,
          'work' => isset($requiredWork) ? $requiredWork : '',
          'isAllTyres' => \Insim\Types\PitWork::isAllTyres($packet->Work) ? true : false,
          'isRefuel' => \Insim\Types\PitWork::isRefuel($packet->Work) ? true : false,
          'workDone' => isset($workDone) ? $workDone : '',
          ]); */
    }

    public function playerPitFinished(isPSF $packet) {
        
    }

    public function playerPitLane(isPLA $packet) {
        //Debug::dump($packet);
    }

    /**
     * Check required pit stops missed windows 
     * @param type $currentLap
     * @param type $player
     * @param type $pitNum
     * @return boolean
     */
    public function checkRequiredPitWindows($currentLap, $player, &$pitNum = false) {
        $pits = (int) $this->host->settings->get('race-pits');

        // Required pits check
        if ($pits == 0)
            return true;

        $pitWindows = $this->host->settings->getPitWindows();
        $pitStopsDone = $player->pitstops;

        //Debug::dump($pitWindows);
        //Debug::dump($pitStopsDone);

        foreach ($pitWindows as $pitNumber => $window) {
            if ($window['to'] > 0 && $currentLap >= $window['to'] && (!isset($pitStopsDone) OR @ !$pitStopsDone[$pitNumber])) {
                // Mark as done but give penalty
                $pitNum = $pitNumber;
                return false;
            }
        }

        return true;
    }

    // Events
    /**
     * Called when host settings has changed
     * @param type $key
     * @param type $value
     */
    public function eventHostSettingChanged($key, $value) {

        // Update players UI
        if (isset($this->players[$this->host->id])) {
            foreach ($this->players[$this->host->id] as $player) {
                $player->ui->eventHostSettingChanged($key, $value);
            }
        }
    }

    /**
     * Called when host counter starts
     */
    public function eventCounterStarted($secs, $show = false, $delay = 0, $title = '') {

        // Update players UI
        if (isset($this->players[$this->host->id])) {
            foreach ($this->players[$this->host->id] as $player) {
                $player->ui->showUICounter($secs, $show, $delay, $title);
            }
        }
    }

    /**
     * Called when race started
     */
    public function eventRaceStarted() {
        $this->raceStats->eventRaceStarted($this->host->splitsCount);
        $this->host->safetyService->eventRaceRestarted();

        // Update players UI
        if (isset($this->players[$this->host->id])) {
            foreach ($this->players[$this->host->id] as $player) {
                $player->lapsService->eventRaceStarted();
                $player->ui->eventRaceStarted();
                $player->pitstops = array();    // Reset done pitstops
            }
        }
    }

    /**
     * Resend typed user commands to all other players on server
     * @param type $string
     * @param type $playerName
     */
    public function resendCommand($string, $playerName) {
        if (isset($this->players[$this->host->id])) {
            foreach ($this->players[$this->host->id] as $player) {
                $player->sendMsg($playerName . MsgTypes::WHITE . ' : ' . MsgTypes::GRAY . $string);
            }
        }
    }

    /**
     * Called when player leave spectators
     * @param PlayerClass $player
     */
    public function eventPlayerJoinedTrack(PlayerClass $player) {
        // Check if player can join
        if (!$this->host->canJoinTrack) {
            $this->host->sendMsgPlayer('Do not join track now, please wait.', $player->UCID, MsgTypes::WARNING);
            $this->host->sendCmd("/spec {$player->UName}");
        }

        $this->host->safetyService->eventPlayerJoinedTrack($player);
    }

    /**
     * Called when new player joined
     * @param PlayerClass $player
     */
    public function eventNewConnection(PlayerClass $player) {
        // LFSW PB Request
        $this->host->insim->requestsService->LFSWpbRequest($this->host->id, $player);
        
        // LFSW licence player
        $this->host->insim->requestsService->LFSWlicenceRequest($this->host->id, $player);
    }

    /**
     * Called when player false starts
     */
    public function eventPlayerFalseStart(isPEN $packet) {
        $player = $this->findPlayerByPLID($this->host->id, $packet->PLID);

        if (!$player)
            return;

        $falseStartAction = $this->host->settings->get('race-false-start');

        if ($falseStartAction == FalseStartAction::PIT) {
            $this->host->sendPlayerToPit($player->UName);
            $this->host->clearPlayerPenalty($player->UName);
        }
    }

    // Player management
    public function penalty($params) {
        //TODO
        Debug::dump($params);
    }

    public function spec($params) {
        $username = trim($params['UName']);

        if (!isset($params['UCID'])) {
            $where = array('UName' => $params['UName'], 'host_id' => $this->host->id);
            $conn = $this->getBy($where, $this->tableConns);

            if (!$conn) {
                $this->host->sendMsgPlayer('Player name not found', $params['by_ucid']);
                return;
            }

            $params['UCID'] = $conn->UCID;
        }

        $this->host->sendMsgPlayer('You have been spectated', $params['UCID'], MsgTypes::WARNING);
        $this->host->sendCmd("/spec {$username}");
    }

    public function kick($params) {
        $username = trim($params['UName']);

        if (!isset($params['UCID'])) {
            $where = array('UName' => $params['UName'], 'host_id' => $this->host->id);
            $conn = $this->getBy($where, $this->tableConns);

            if (!$conn) {
                $this->host->sendMsgPlayer('Player name not found', $params['by_ucid']);
                return;
            }

            $params['UCID'] = $conn->UCID;
        }

        $this->host->sendMsgPlayer('You have been kicked', $params['UCID'], MsgTypes::WARNING);
        $this->host->sendCmd("/kick {$username}");
    }

    public function ban($params) {
        $conn = $this->getBy(array('UName' => $params['UName'], 'host_id' => $this->host->id), $this->tableConns);

        $username = $params['UName'];
        $duration = $params['duration'];
        $duration_string = $duration > 0 ? $duration . ' days' : '12 hours';

        // Test if player is online
        if ($conn) {
            $this->host->sendMsgPlayer("You have been banned for {$duration_string}", $conn->UCID, MsgTypes::WARNING);
        } else {
            // Test if player was ever connected to server
            $player = $this->getBy(array('UName' => $params['UName']), $this->tablePlayers);
            if (!$player) {
                $this->host->sendMsgPlayer('Player name not found', $params['by_ucid']);
                return;
            }

            $this->host->sendMsgPlayer("Player has been banned for {$duration_string}", $params['by_ucid']);
        }

        $this->host->sendCmd("/ban {$username} {$duration}");

        // Create new ban or update already existing
        $ban = $this->getBy(array('UName' => $params['UName']), $this->tablePlayersBan);
        if (!$ban) {
            $ban = new PlayerBan();
        }

        $ban->UName = $params['UName'];
        $ban->banned = 1;
        $ban->banned_by = $params['by_id'];
        $ban->banned_date = date('Y-m-d H:i:s');
        $ban->unbanned_by = null;
        $ban->unbanned_date = null;
        $ban->duration = $duration;
        $ban->reason = isset($params['reason']) ? $params['reason'] : '';

        $this->save($ban, $this->tablePlayersBan);
    }

    public function unban($params) {
        $ban = $this->getBy(array('UName' => $params['UName']), $this->tablePlayersBan);

        if (!$ban) {
            $this->host->sendMsgPlayer('Player name not found', $params['by_ucid']);
            return;
        }

        if ($ban->banned != 1) {
            $this->host->sendMsgPlayer('Player is already unbanned', $params['by_ucid']);
            return;
        }

        $username = $params['UName'];
        $this->host->sendCmd("/unban {$username}");

        $ban->banned = 0;
        $ban->unbanned_date = date('Y-m-d H:i:s');
        $ban->unbanned_by = $params['by_id'];
        $this->save($ban, $this->tablePlayersBan);

        $this->host->sendMsgPlayer('Player has been unbanned', $params['by_ucid'], MsgTypes::NOTIFY);
    }

    public function message() {
        
    }

    /**
     * ACL commands
     * @param type $params
     */
    public function aclCmd($params) {
        switch ($params['cmd']) {
            case 'add': $this->addAdmin($params);
                return;
            case 'remove': $this->removeAdmin($params);
                return;
        }
    }

    /**
     * Add admin or change player role
     * @param type $params
     */
    public function addAdmin($params) {
        $admin = $this->ACLService->getByLicence($params['UName']);
        $roles = ACLService::getRoles();

        if (!in_array($params['role'], $roles)) {
            $this->host->sendMsgPlayer("{$params['role']} is undefined role", $params['by_ucid'], MsgTypes::YELLOW);
            return;
        }

        if ($admin) {
            if ($admin->role == $params['role']) {
                $this->host->sendMsgPlayer("{$params['UName']} is already {$params['role']}", $params['by_ucid'], MsgTypes::YELLOW);
            } else {
                $admin->role = $params['role'];
                $this->host->sendMsgPlayer("{$params['UName']} role was changed to {$params['role']}", $params['by_ucid'], MsgTypes::YELLOW);
            }
        } else {
            $admin = new PlayerACL();
            $admin->UName = $params['UName'];
            $admin->role = $params['role'];
            $this->host->sendMsgPlayer("{$params['UName']} was added as {$params['role']}", $params['by_ucid'], MsgTypes::YELLOW);
        }

        $this->ACLService->save($admin);
    }

    /**
     * Remove admin
     * @param type $params
     */
    public function removeAdmin($params) {
        $admin = $this->ACLService->getByLicence($params['UName']);

        if ($admin) {
            $this->ACLService->delete($admin->id);
            $this->host->sendMsgPlayer("{$params['UName']} is no longer admin", $params['by_ucid'], MsgTypes::YELLOW);
        } else {
            $this->host->sendMsgPlayer("{$params['UName']} is not admin", $params['by_ucid'], MsgTypes::YELLOW);
        }
    }

    // DB functions

    /**
     * Save or update connection
     * @param isNCN $packet
     * @return type
     */
    public function saveConn(isNCN $packet) {
        $data = $packet->exchangeObject();
        $data['host_id'] = $this->host->id;
        $data['PName_utf'] = LfsString::convertWithoutColor($packet->PName);
        $data['added'] = date('Y-m-d H:i:s');
        $where = array('UCID' => $packet->UCID, 'host_id' => $this->host->id);

        $conn = $this->getBy($where, $this->tableConns);

        if ($conn) {
            $this->tableConns->update($data, $where);
        } else {
            $this->tableConns->insert($data);
        }
        $data['country'] = '';
        $data['gmt'] = '';
        $data['lang'] = 0;

        // Players DB
        $playerDB = $this->getPlayer(array('UName' => $packet->UName), $this->tablePlayers);
        if ($playerDB) {
            $playerDB->PName = $packet->PName;
            $playerDB->PName_utf = $data['PName_utf'];
            $playerDB->last_joined = date('Y-m-d H:i:s');
            $data['country'] = $playerDB->country;
            $data['gmt'] = $playerDB->gmt;
            $data['role'] = $playerDB->role;
            $this->tablePlayers->update($playerDB->exchangeObject(), array('UName' => $packet->UName));
        } else {
            $playerDB = new Player();
            $playerDB->UName = $packet->UName;
            $playerDB->PName = $packet->PName;
            $playerDB->PName_utf = $data['PName_utf'];
            $playerDB->last_joined = date('Y-m-d H:i:s');
            $this->tablePlayers->insert($playerDB->exchangeObject());
            $playerDB->id = $this->tablePlayers->lastInsertValue;
        }

        // Run-time players list
        if (!isset($this->players[$this->host->id][$packet->UCID])) {
            $player = new PlayerClass($packet->UCID);
            $player->UCID = $packet->UCID;
            $player->name = $packet->PName;
            $player->UName = $packet->UName;
            $player->local = !$packet->isRemote();
            $player->host = &$this->host;
            if ($player->local)
                $player->host->localUCID = $player->UCID;
            $player->player_id = $playerDB->id;
            $player->acl = clone $this->ACLService;
            $player->acl->init($player);
            $player->ui = clone $this->UIService;
            $player->ui->player = &$player;
            $player->commandService = clone $this->commandService;
            $player->commandService->player = &$player;
            $player->lapsService = clone $this->lapsService;
            $player->lapsService->player = &$player;
            $player->timecompareService = clone $this->timecompareService;
            $player->timecompareService->player = &$player;
            $player->playerService = &$this;
            $player->settings = clone $this->playerSttingsService;
            $player->settings->player_id = $player->player_id;
            $player->settings->default_id = $this->host->silent ? -1 : 0;
            $player->settings->load();
            $player->translator = clone $this->translator;

            $this->players[$this->host->id][$packet->UCID] = $player;

            if (!isset($data['role'])) {
                $data['role'] = $player->acl->playerRole;
            }

            // Events
            $this->eventNewConnection($player);
            $player->ui->eventNewConn();

            foreach ($this->players[$this->host->id] as $player) {
                if (isset($player->ui->ui_list['playerList']) && $player->ui->ui_list['playerList']->displayed) {
                    $player->ui->ui_list['playerList']->updatePlayersAdd($data);
                }
            }
        }
    }

    /**
     * Save connection info
     * @param isNCI $packet
     * @return type
     */
    public function saveConnInfo(isNCI $packet) {
        $where = array('UCID' => $packet->UCID, 'host_id' => $this->host->id);
        $conn = $this->getBy($where, $this->tableConns);

        if (!$conn) {
            return;
        }

        $conn->lang = $packet->Language;
        $conn->UserID = $packet->UserID;
        $conn->IPAddress = $packet->IPAddress;

        $data = $conn->exchangeObject();
        $this->tableConns->update($data, $where);

        // Players DB
        $playerDB = $this->getBy(array('UName' => $conn->UName), $this->tablePlayers);

        if ($playerDB) {
            // Request IP geo lookup if IP changed
            //if (empty($playerDB->country) OR empty($playerDB->timezone) OR $playerDB->IPAddress != $packet->IPAddress)
            $this->host->insim->requestsService->ipLocationRequest($this->host->id, $conn->UName, $packet->IPAddress);
            if (empty($playerDB->gmt) && !empty($playerDB->timezone)) {
                $date = new DateTime();
                try {
                    $date->setTimezone(new DateTimeZone($playerDB->timezone));
                    $playerDB->gmt = $date->format('P');
                } catch (Exception $ex) {
                    $playerDB->gmt = '';
                }
            }

            $playerDB->lang = $packet->Language;
            $playerDB->IPAddress = $packet->IPAddress;
            $playerDB->UserID = $packet->UserID;
            $this->tablePlayers->update($playerDB->exchangeObject(), array('id' => $playerDB->id));

            // Set tranlator locale and show welcome messages
            if (isset($this->players[$this->host->id][$packet->UCID])) {
                $this->players[$this->host->id][$packet->UCID]->translator->setLanguage($packet->Language);
                $this->players[$this->host->id][$packet->UCID]->ui->eventWelcome();
            }

            // Update UI
            foreach ($this->players[$this->host->id] as $player) {
                if (isset($player->ui->ui_list['playerList']) && $player->ui->ui_list['playerList']->displayed) {
                    $player->ui->ui_list['playerList']->updatePlayersCountry($playerDB);
                }
            }
        }
    }

    /**
     * Save resolved country / timezone by IP
     * @param type $host_id
     * @param type $UName
     * @param type $json
     */
    public function saveIP($host_id, $UName, $json) {
        // Players DB
        $playerDB = $this->getBy(array('UName' => $UName), $this->tablePlayers);
        if ($playerDB) {
            $playerDB->country = $json['country'];
            $playerDB->timezone = $json['timezone'];
            $playerDB->isp = $json['isp'];

            $date = new DateTime();
            try {
                $date->setTimezone(new DateTimeZone($json['timezone']));
                $playerDB->gmt = $date->format('P');
            } catch (Exception $ex) {
                $playerDB->gmt = '';
            }


            $this->tablePlayers->update($playerDB->exchangeObject(), array('id' => $playerDB->id));
        }

        // Update UI
        if (isset($this->players[$host_id])) {
            foreach ($this->players[$host_id] as $player) {
                if (isset($player->ui->ui_list['playerList']) && $player->ui->ui_list['playerList']->displayed) {
                    $player->ui->ui_list['playerList']->updatePlayersCountry($playerDB);
                }
            }
        }

        $this->log('IP Lookup success (' . $UName . ')');
    }
    
    public function saveLFSWLicence($host_id, $UName, $licence) {        
        // Players DB
        $playerDB = $this->getBy(array('UName' => $UName), $this->tablePlayers);
        if ($playerDB) {
            $playerDB->licence = $licence;
            $this->tablePlayers->update($playerDB->exchangeObject(), array('id' => $playerDB->id));
        }

        // Update UI
        if (isset($this->players[$host_id])) {
            foreach ($this->players[$host_id] as $player) {
                if (isset($player->ui->ui_list['playerList']) && $player->ui->ui_list['playerList']->displayed) {
                    $player->ui->ui_list['playerList']->updatePlayersLicence($playerDB);
                }
            }
        }

        $this->log('LFSW licence request success (' . $UName . ')');
    }

    /**
     * Delete connection
     * @param isCNL $packet
     */
    public function deleteConn(isCNL $packet) {
        $where = new Where();
        $where->equalTo('UCID', $packet->UCID);
        $where->equalTo('host_id', $this->host->id);

        $this->tableConns->delete($where);

        $this->removePlayer($this->host->id, $packet);
    }

    /**
     * Delete all connection and players from server
     * @param Host $host
     */
    public function clearConnected() {
        if (!isset($this->host))
            return;

        $where = new Where();
        $where->equalTo('host_id', $this->host->id);

        $this->tableConns->delete($where);
        $this->tableGateway->delete($where);
    }

    public function getCurrentConns() {
        $sql = new Sql($this->tableConns->getAdapter());

        $select = $sql->select();
        $select->from('hosts_conns');

        $select->join(array('pl' => 'hosts_players'), new Expression('hosts_conns.UCID = pl.UCID AND pl.host_id = hosts_conns.host_id'), array(
            'PLID' => 'PLID',
            'PType' => 'PType',
            'Flags' => 'Flags',
            'Flags_string' => 'Flags_string',
            'CName' => 'CName',
            'SetF' => 'SetF',
                ), Select::JOIN_LEFT);

        $select->join(array('pldb' => 'players'), 'hosts_conns.UName = pldb.UName', array('country' => 'country', 'gmt' => 'gmt', 'licence' => 'licence'), Select::JOIN_LEFT);
        $select->join(array('acl' => 'players_acl'), 'hosts_conns.UName = acl.UName', array('role' => 'role'), Select::JOIN_LEFT);

        //$select->where->and->equalTo('pl.host_id', $this->host->id);
        $select->where->and->equalTo('hosts_conns.host_id', $this->host->id);

        $select->order('hosts_conns.PName_utf ASC');

        $resultSet = $this->tableConns->selectWith($select);
        $resultSet->buffer();

        return $resultSet;
    }

    public function getCurrentConnsSimple($columns = array('*')) {
        $resultSet = $this->tableConns->select(function(Select $select) use($columns) {
            $select->where->equalTo('host_id', $this->host->id);
            $select->columns($columns);
        });

        $resultSet->buffer();

        return $resultSet;
    }

    public function getCurrentPlayers($columns = array('*')) {
        $resultSet = $this->tableGateway->select(function(Select $select) use($columns) {
            $select->where->equalTo('host_id', $this->host->id);
            $select->columns($columns);
        });

        $resultSet->buffer();

        return $resultSet;
    }

    public function getPlayer($where = array()) {
        $sql = new Sql($this->tablePlayers->getAdapter());

        $select = $sql->select();
        $select->from('players');

        foreach ($where as $key => $value) {
            $select->where->and->equalTo('players.' . $key, $value);
        }

        $select->join(array('acl' => 'players_acl'), 'players.UName = acl.UName', array('role' => 'role'), Select::JOIN_LEFT);

        $resultSet = $this->tablePlayers->selectWith($select);

        return $resultSet->current();
    }

    /**
     * Get recently disconnected players
     * @return PlayerRecent
     */
    public function getRecentPlayers() {
        $resultSet = $this->tablePlayersRecent->select(function(Select $select) {
            //$select->where->and->equalTo($key, $value);

            $select->limit(200);
            $select->order('date DESC');
        });

        $resultSet->buffer();

        return $resultSet;
    }

    public function setConnsTable(TableGateway $tableGateway) {
        $this->tableConns = $tableGateway;
    }

    public function setPlayersTable(TableGateway $tableGateway) {
        $this->tablePlayers = $tableGateway;
    }

    public function setPlayersBanTable(TableGateway $tableGateway) {
        $this->tablePlayersBan = $tableGateway;
    }

    public function setPlayersRecentTable(TableGateway $tableGateway) {
        $this->tablePlayersRecent = $tableGateway;
    }

}
