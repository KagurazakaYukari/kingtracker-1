<?php

namespace Insim\Service;

use Application\Service\CoreService;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Select;
use Zend\Db\TableGateway\TableGateway;

class TaskService extends CoreService {

    public function __construct(TableGateway $tableGateway) {
        parent::__construct($tableGateway);
        $this->debug = true;
    }

    public function getCount($task = null) {
        $resultSet = $this->tableGateway->select(function (Select $select) use($task) {
            if ($task)
                $select->where->equalTo('task', $task);

            $select->columns(array(
                'count' => new Expression('COUNT(id)')
            ));
        });

        return intval($resultSet->current()->count);
    }

    public function addTask($action) {
        $count = $this->getCount($action);
        
        if ($count == 0) {
            $task = new \Insim\Model\Task();
            $task->task = $action;
            $this->save($task);
        }
    }

}
