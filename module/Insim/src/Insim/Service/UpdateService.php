<?php

namespace Insim\Service;

use Application\Service\CoreService;
use cURL\Request;
use DirectoryIterator;
use Exception;
use PDO;
use Zend\Config\Config;
use Zend\Config\Writer\PhpArray;
use ZipArchive;

class UpdateService extends CoreService {

    const UPDATE_URL = 'https://kingtracker-1217.appspot.com/update';
    const UPDATE_URL_DOWNLOAD = 'https://storage.googleapis.com/kingtracker-1217.appspot.com/updates/';
    const UPDATE_DIR = '/update';
    // TODO DB param
    const UPDATE_TIME = '03:00';

    public $paramsService;
    public $cid;
    public $key;

    public function __construct() {
        $this->debug = true;
    }

    public function updateRequest() {
        $this->cid = $this->paramsService->getBy(array('name' => 'cid'))->value;
        $this->key = $this->paramsService->getBy(array('name' => 'key'))->value;
        $automatic_update = $this->paramsService->getBy(array('name' => 'automatic_update'))->value;
        
        if(!$automatic_update){
            return null;
        }

        $token = hash_hmac('SHA512', $this->cid, $this->key);

        if (!empty($this->cid) && !empty($this->key)) {
            $query = http_build_query(array(
                'cid' => $this->cid,
                'token' => $token
            ));
            return new Request(UpdateService::UPDATE_URL . '?' . $query);
        }

        return null;
    }

    public function validateResponse($json, &$request = array()) {
        if (!isset($json['hash']))
            return false;

        $hash = $json['hash'];
        unset($json['status']);
        unset($json['hash']);

        $isValid = hash_hmac('SHA512', json_encode($json), $this->key) == $hash;

        if ($isValid) {
            $currentVersion = intval($json['version']);

            if ($currentVersion && $currentVersion > VERSION_INT) {
                // Direct URL download
                if (isset($json['url']) && !empty($json['url'])) {
                    $request[] = array(
                        'path' => BASE_PATH . UpdateService::UPDATE_DIR . "/update_{$currentVersion}.zip",
                        'fileName' => "update_{$currentVersion}.zip",
                        'url' => $json['url']
                    );
                    return $isValid;
                }

                // Incremental version update
                $toUpdate = $currentVersion - VERSION_INT;

                for ($i = (VERSION_INT + 1); $i <= $currentVersion; $i++) {
                    $request[] = array(
                        'path' => BASE_PATH . UpdateService::UPDATE_DIR . "/update_{$i}.zip",
                        'fileName' => "update_{$i}.zip",
                        'url' => UpdateService::UPDATE_URL_DOWNLOAD . "update_{$i}.zip"
                    );
                }
            }
        }

        return $isValid;
    }

    public function installUpdates() {
        $update_path = BASE_PATH . UpdateService::UPDATE_DIR;
        $destination = BASE_PATH . UpdateService::UPDATE_DIR . '/temp/';

        $zipFiles = array();
        $dir = new DirectoryIterator($update_path);

        foreach ($dir as $file) {
            if ($file->isFile() && $file->getExtension() == 'zip') {
                $zipFiles[] = array(
                    'fileName' => $file->getFilename(),
                    'filePath' => $file->getRealPath()
                );
            }
        }

        asort($zipFiles);

        if (count($zipFiles) > 0) {
            $this->log('Installing updates');
        } else {
            $this->log('Nothing to update');
        }

        foreach ($zipFiles as $file) {
            // Extract and copy files
            $this->log('Installing: ' . $file['fileName']);
            if ($this->extractZip($file['filePath'], $destination)) {
                // Run database updates
                $dbErrors = array();
                $this->updateDB($dbErrors);

                $this->xcopy($destination, BASE_PATH);
            }

            // CleanUp
            unlink($file['filePath']);
            $this->rrmdir($destination);
        }

        if (count($zipFiles) > 1) {
            $this->log('Installation done');
            $this->log('Restarting ...');
        }

        exit();
    }

    public function updateDB(&$errors = array()) {
        $this->log('Updating database');
        $destination = BASE_PATH . UpdateService::UPDATE_DIR . '/temp/';

        $config = new Config(include BASE_PATH . '/config/autoload/local.php');
        $options = array(
            PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
        );

        try {
            $dns = $config->db->dsn;
            $db = new PDO($dns, $config->db->username, $config->db->password, $options);
            $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (Exception $ex) {
            $errors[] = 'Unable to connect to database.';
            return false;
        }

        $dir = new DirectoryIterator($destination);

        foreach ($dir as $file) {
            if ($file->isFile() && $file->getExtension() == 'sql') {
                $lines = array();
                $templine = '';

                $handle = fopen($file->getRealPath(), "r");
                if ($handle) {
                    while (($line = fgets($handle)) !== false) {
                        $lines[] = $line;
                    }

                    fclose($handle);
                }

                foreach ($lines as $line) {
                    if (substr($line, 0, 2) == '--' || $line == '')
                        continue;

                    $templine .= $line;
                    if (substr(trim($line), -1, 1) == ';') {
                        try {
                            $db->query($templine);
                        } catch (Exception $ex) {
                            $errors[] = $ex->getMessage();
                        }

                        $templine = '';
                    }
                }

                // Delete file
                unlink($file->getRealPath());
            }
        }

        $db = null;

        if (count($errors) < 1) {
            return true;
        } else {
            $log = fopen(BASE_PATH . '/logs/update_errors_' . (date('d.m.Y')) . '.txt', 'a');
            foreach ($errors as $string) {
                fwrite($log, $string . PHP_EOL);
            }
            fclose($log);
            return false;
        }
    }

    /**
     * Extract zip archive
     * @param type $zipFile
     * @return boolean
     */
    private function extractZip($zipFile, $destination) {

        $zip = new ZipArchive;
        if ($zip->open($zipFile) === TRUE) {
            $zip->extractTo($destination);
            $zip->close();
            return true;
        } else {
            return false;
        }
    }

    /**
     * Copy a file, or recursively copy a folder and its contents
     * @author      Aidan Lister <aidan@php.net>
     * @version     1.0.1
     * @link        http://aidanlister.com/2004/04/recursively-copying-directories-in-php/
     * @param       string   $source    Source path
     * @param       string   $dest      Destination path
     * @param       string   $permissions New folder creation permissions
     * @return      bool     Returns true on success, false on failure
     */
    private function xcopy($source, $dest, $permissions = 0755) {
        // Check for symlinks
        if (is_link($source)) {
            return symlink(readlink($source), $dest);
        }

        // Simple copy for a file
        if (is_file($source)) {
            return copy($source, $dest);
        }

        // Make destination directory
        if (!is_dir($dest)) {
            mkdir($dest, $permissions);
        }

        // Loop through the folder
        $dir = dir($source);
        while (false !== $entry = $dir->read()) {
            // Skip pointers
            if ($entry == '.' || $entry == '..') {
                continue;
            }

            // Deep copy directories
            $this->xcopy("$source/$entry", "$dest/$entry", $permissions);
        }

        // Clean up
        $dir->close();
        return true;
    }

    /**
     * Recursively delete folders and files
     * @param type $dir
     */
    private function rrmdir($dir) {
        if (is_dir($dir)) {
            $objects = scandir($dir);
            foreach ($objects as $object) {
                if ($object != "." && $object != "..") {
                    if (is_dir($dir . "/" . $object))
                        $this->rrmdir($dir . "/" . $object);
                    else
                        unlink($dir . "/" . $object);
                }
            }
            rmdir($dir);
        }
    }

}
