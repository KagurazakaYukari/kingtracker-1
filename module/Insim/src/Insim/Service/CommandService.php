<?php

namespace Insim\Service;

use Application\Service\CoreService;
use Insim\Packets\isIII;
use Insim\Packets\isMSO;
use Insim\Packets\Packet;
use Insim\Types\MsgTypes;

class CommandService extends CoreService {

    public $player;
    public $commands;
    protected $tableConns;
    protected $prefix = '!';

    public function __construct() {
        $this->debug = true;

        $this->commands = include BASE_PATH . '/config/insim.commands.php';
    }

    public function handlePacket($header, $rawPacket) {
        switch ($header['Type']) {
            case Packet::ISP_MSO:
                $mso = new isMSO($rawPacket);
                
                switch ($mso->UserType) {
                    case isMSO::MSO_USER:
                    case isMSO::MSO_PREFIX:
                    case isMSO::MSO_PREFIX:
                    case isMSO::MSO_O:
                        $this->processUserCommands($mso);
                        break;
                    case isMSO::MSO_SYSTEM:
                        $this->processSystemMessages($mso);

                        if ($this->player->host->local) {
                            $this->processUserCommands($mso);
                        }
                        break;
                }

                break;
            case Packet::ISP_III:
                $iii = new isIII($rawPacket);
                $this->processHiddenCommands($iii);
                break;
        }
    }

    /**
     * Process users commands
     * @param isMSO $message
     * @return type
     */
    public function processUserCommands(isMSO $message) {        
        if ($message->UCID != $this->player->UCID) {
            if (!$this->player->host->local)
                return;
        }
        
        $prefix = $this->player->host->cmd ? $this->player->host->cmd : $this->prefix;

        $string_original = substr($message->Msg, $message->TextStart);
        $string = strtolower(substr($message->Msg, $message->TextStart));

        if (substr($string, 0, 1) != $prefix) {
            return;
        }

        if ($string == $prefix) {
            $this->player->ui->showHelpAll();
        }

        foreach ($this->commands as $key => $command) {
            if (strpos($string, $prefix . $key) !== false) {
                if ($this->player->acl->check($key)) {
                    $params = $this->parseParams($key, trim($string_original), $command);
                    if ($params !== FALSE) {
                        $params['by_id'] = $this->player->player_id;
                        $params['by_ucid'] = $this->player->UCID;
                        $command['callback']($this, $params);

                        // Resend command as message
                        if (@!$command['noresend'] && !$this->player->host->local) {
                            $this->player->playerService->resendCommand($string, $this->player->name);
                        }
                    }
                } else {
                    $this->player->acl->accessDenied();
                }
            }
        }
    }

    /**
     * Process hidden user commands /i
     * @param isIII $message
     * @return type
     */
    public function processHiddenCommands(isIII $message) {
        if ($message->UCID != $this->player->UCID) {
            if (!$this->player->host->local)
                return;
        }

        $prefix = $this->player->host->cmd ? $this->player->host->cmd : $this->prefix;
        $string = $message->Msg;

        if (substr($string, 0, 1) != $prefix) {
            return;
        }

        foreach ($this->commands as $key => $command) {
            if (strpos($string, $prefix . $key) !== false) {
                if ($this->player->acl->check($key)) {
                    $params = $this->parseParams($key, trim($string), $command);
                    if ($params !== FALSE) {
                        $params['by_id'] = $this->player->player_id;
                        $params['by_ucid'] = $this->player->UCID;
                        $command['callback']($this, $params);
                    }
                } else {
                    $this->player->acl->accessDenied();
                }
            }
        }
    }

    public function processSystemMessages(isMSO $mso) {
        if (strpos($mso->Msg, 'Track loaded') !== false) {
            $this->player->host->trackLoaded();
        }
    }

    public function parseParams($cmd, $string, $command) {
        unset($command['callback']);

        $prefix = $this->player->host->cmd ? $this->player->host->cmd : $this->prefix;
        
        preg_match_all('/"(?:\\\\.|[^\\\\"])*"|\S+/', $string, $matches);
        if ($matches[0] !== null) {
            $params = $matches[0];
        }

        if ($params[0] != $prefix . $cmd OR ( count($params) - 1) < $command['required']) {
            $this->player->sendTranslatedMsg('MSG_CMD_FORMAT', $color = MsgTypes::NORMAL, ['msg' => $command['params']]);
            return false;
        }

        $return = array();

        if (isset($command['format'])) {
            $index = 1;
            foreach ($command['format'] as $key => $type) {
                if (isset($params[$index])) {
                    switch ($type) {
                        case 'string':
                            $return[$key] = $this->parseString($params[$index]);
                            break;
                        case 'int':
                            $return[$key] = $this->parseInt($params[$index]);
                            break;
                    }
                    $index++;
                }
            }
        }
        return $return;
    }

    public function parseString($value) {
        return trim($value);
    }

    public function parseInt($value) {
        return intval(trim($value));
    }

}
