<?php

namespace Insim\Service;

use Application\Service\CoreService;
use DirectoryIterator;
use Insim\Packets\isAXI;
use LfsString;

class LayoutService extends CoreService {

    public $host;
    public $layouts;
    public $isLoading = false;
    public $currentLayout = '';
    public $callBacks = [];

    public function __construct() {
        $this->debug = true;
    }

    public function init() {
        // Load available layouts
        if (!isset($this->host->config['lytdir']) OR empty($this->host->path)) {
            $this->log('Unable to load available layout (' . $this->host->name . ')');
            return false;
        }

        $dir = new DirectoryIterator("{$this->host->path}data/{$this->host->config['lytdir']}");

        if ($dir) {
            foreach ($dir as $file) {
                if ($file->isFile() && $file->getExtension() == 'lyt') {
                    $layout = explode('_', $file->getBasename('.lyt'), 2);

                    if (count($layout) == 2 && !empty($layout[1])) {
                        $this->layouts[$layout[0]][] = $layout[1];
                    }
                }
            }
        }
    }

    /**
     * Get available layout for current track
     * @param type $track
     * @return type
     */
    public function getAvailableLayouts($track = null) {
        $track = isset($track) ? $track : $this->host->Track;

        return isset($this->layouts[$track]) ? $this->layouts[$track] : [];
    }
    
    /**
     * Get all layouts by track code (BL, WE ...)
     * @param type $trackCode
     * @return type
     */
    public function getAvailableLayoutsByCode($trackCode){        
        $return = [];
        foreach($this->layouts as $openTrack => $layouts){
            if (strpos($openTrack, $trackCode) === 0 && count($layouts) > 0){
                $return[$openTrack] = $layouts;
            }
        }
        return $return;
    }

    /**
     * Change layout
     * @param type $name
     * @param type $callBack
     * @return boolean
     */
    public function changeLayout($name, $callBack = null) {
        if ($this->isLoading) {
            return false;
        }

        $this->isLoading = true;
        
        if($callBack)
            $this->callBacks[] = $callBack;

        $this->host->sendCmd("/axload " . LfsString::stripColor($name));
        $this->host->sendMsgHost("> Loading layout: {$name}", \Insim\Types\MsgTypes::YELLOW);
    }

    /**
     * Clear layout
     * @param type $callBack
     * @return boolean
     */
    public function clearLayout($callBack = null) {
        if ($this->isLoading) {
            return false;
        }

        $this->host->sendCmd('/axclear');

        // Request layout info
        $tiny_axi = new \Insim\Packets\isTINY();
        $tiny_axi->ReqI = true;
        $tiny_axi->SubT = \Insim\Packets\isTINY::TINY_AXI;
        $this->host->insim->send($tiny_axi);

        $this->isLoading = false;
        
        if($callBack)
            $this->callBacks[] = $callBack;
        
        $this->callBacks[] = function() {
            // Delayed request race info (update splits count)
            $this->host->addTimer(new \Insim\Model\Timer('request_RST', 3, function() {
                $tiny_rst = new \Insim\Packets\isTINY();
                $tiny_rst->ReqI = true;
                $tiny_rst->SubT = \Insim\Packets\isTINY::TINY_RST;
                $this->host->insim->send($tiny_rst);
            }));
        };
    }

    /**
     * Called when layout changes
     * @param isAXI $packet
     */
    public function eventLayoutChanged(isAXI $packet) {
        $this->currentLayout = $packet->LName;
        $this->isLoading = false;

        if (count($this->callBacks) > 0) {
            foreach ($this->callBacks as $key => $callback) {
                call_user_func($callback);
                unset($this->callBacks[$key]);
            }
        }
    }

}
