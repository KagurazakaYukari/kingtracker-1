<?php

namespace Insim\Service;

use Application\Service\CoreService;
use Insim\Helper\Math;
use Insim\Model\Host;
use Insim\Model\PlayerClass;
use Insim\Types\MsgTypes;

class SafetyService extends CoreService {

    public $host;

    /* public function __construct(TableGateway $tableGateway) {
      parent::__construct($tableGateway);
      $this->debug = true;
      } */

    /**
     * Idle detection
     */
    public function idleDetection() {
        if (!$this->host->settings->get('safety-idle')) {
            return;
        }

        $idle_time = $this->host->settings->get('safety-idle-time');
        $idle_speed_treshold = $this->host->settings->get('safety-idle-speed');
        $idle_warning_time = (($idle_time - 15) < 10) ? 10 : ($idle_time - 15);

        foreach ($this->host->playerService->players[$this->host->id] as $player) {
            // Car is on track
            if ($player->PLID && $player->compCar) {
                $car = $player->compCar;
                $speed = Math::speedToKph($car->Speed);

                // Start of idle
                if ($speed <= $idle_speed_treshold) {
                    if ($player->idle_time == null) {
                        $player->idle_time = time();
                    }
                } else {
                    $player->idle_time = null;
                    $player->isIdle = false;
                    $player->isIdleWarned = false;
                }

                // Warning
                if ($player->idle_time && !$player->isIdleWarned && (time() - $player->idle_time >= $idle_warning_time)) {
                    $this->eventPlayerIdleWarning($player);
                }

                // Detect idle
                if ($player->idle_time && (time() - $player->idle_time >= $idle_time)) {
                    $this->eventPlayerIsIdle($player);
                } else {
                    $player->isIdle = false;
                }
            }
        }
    }

    // Events
    /**
     * Called when car position updates
     */
    public function eventCarPositionUpdated() {
        $this->idleDetection();
    }

    /**
     * Called when race / qualify starts
     */
    public function eventRaceRestarted() {
        // Reset idle times
        if (isset($this->host->playerService->players[$this->host->id])) {
            foreach ($this->host->playerService->players[$this->host->id] as $player) {
                $player->idle_time = null;
                $player->isIdle = false;
                $player->isIdleWarned = false;
            }
        }
    }

    /**
     * Called wen player joins track
     */
    public function eventPlayerJoinedTrack(PlayerClass &$player) {
        $player->idle_time = null;
        $player->isIdle = false;
        $player->isIdleWarned = false;
    }

    /**
     * Called when player is idle warned
     */
    public function eventPlayerIdleWarning(PlayerClass &$player) {
        if (!($this->host->race_state == Host::QUALIFY_FINISHED OR $this->host->race_state == Host::RACE_FINISHED)) {
            $player->isIdleWarned = true;
            $player->sendTranslatedMsg('MSG_IDLE_WARNING', MsgTypes::YELLOW);
        }
    }

    /**
     * Called when players is idle
     */
    public function eventPlayerIsIdle(PlayerClass &$player) {
        if (!($this->host->race_state == Host::QUALIFY_FINISHED OR $this->host->race_state == Host::RACE_FINISHED)) {
            $player->isIdle = true;
            $this->host->spectatePlayer($player->UName);
        }
    }

}
