<?php

namespace Insim\Packets;

/**
 * IS OCO
 */
class isOCO extends Packet {

    const PACK = 'CCCxCCCC';
    const UNPACK = 'CSize/CType/CReqI/xZero/COCOAction/CIndex/CIdentifier/CData';

    protected $Size = 8;        # 8
    protected $Type = Packet::ISP_OCO;  # ISP_OCO
    protected $ReqI;            # 0
    protected $Zero;
    public $OCOAction;          # see below
    public $Index;              # see below
    public $Identifier;         # identify particular start lights objects (0 to 63 or 255 = all)
    public $Data;               # see below

}
