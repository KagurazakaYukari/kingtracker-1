<?php

namespace Insim\Packets;

/**
 * Pit Stop Finished
 */
class isPSF extends Packet {

    const PACK = 'CCxCVV';
    const UNPACK = 'CSize/CType/CReqI/CPLID/VSTime/VSpare';

    protected $Size = 12;       # 12
    protected $Type = self::ISP_PSF;  # ISP_PSF
    protected $ReqI;            # 0
    public $PLID;               # player's unique id
    public $STime;              # stop time (ms)
    protected $Spare;

}
