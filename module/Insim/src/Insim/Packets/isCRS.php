<?php

namespace Insim\Packets;

/**
 * Car reset packet
 */
class isCRS extends Packet {

    const PACK = 'CCxC';
    const UNPACK = 'CSize/CType/CReqI/CPLID';

    protected $Size = 4;        # 4
    protected $Type = Packet::ISP_CRS;  # ISP_CRS
    protected $ReqI = null;     # 0
    public $PLID;               # player's unique id

}
