<?php

namespace Insim\Packets;

/**
 * Split
 */
class isSPX extends Packet {

    const PACK = 'CCxCVVCCCx';
    const UNPACK = 'CSize/CType/CReqI/CPLID/VSTime/VETime/CSplit/CPenalty/CNumStops/CSp3';

    protected $Size = 16;       # 16
    protected $Type = Packet::ISP_SPX;  # ISP_SPX
    protected $ReqI = null;     # 0
    public $PLID;               # player's unique id
    public $STime;              # split time (ms)
    public $ETime;              # total time (ms)
    public $Split;              # split number 1, 2, 3
    public $Penalty;            # current penalty value (see below)
    public $NumStops;           # number of pit stops
    protected $Sp3;

    public function __conStruct($rawPacket = null) {
        parent::__conStruct($rawPacket);
    }

}
