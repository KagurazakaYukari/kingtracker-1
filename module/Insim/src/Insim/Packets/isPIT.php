<?php

namespace Insim\Packets;

/**
 * PIT stop (stop at pit garage)
 */
class isPIT extends Packet {

    const PACK = 'CCxCvvxCCxC4VV';
    const UNPACK = 'CSize/CType/CReqI/CPLID/vLapsDone/vFlags/CSp0/CPenalty/CNumStops/CSp3/C4Tyres/VWork/VSpare';

    protected $Size = 24;       # 24
    protected $Type = self::ISP_PIT;  # ISP_PIT
    protected $ReqI = null;     # 0
    public $PLID;               # player's unique id
    public $LapsDone;           # laps completed
    public $Flags;              # player flags
    protected $Sp0;
    public $Penalty;            # current penalty value (see below)
    public $NumStops;           # number of pit stops
    protected $Sp3;
    public $Tyres = array();    # tyres changed
    public $Work;               # pit work
    protected $Spare;

    public function unpack($rawPacket) {
        parent::unpack($rawPacket);

        for ($Tyre = 1; $Tyre <= 4; ++$Tyre) {
            $Property = "Tyres{$Tyre}";
            $this->Tyres[] = $this->$Property;
            unset($this->$Property);
        }

        return $this;
    }

}
