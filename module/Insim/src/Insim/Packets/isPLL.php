<?php

namespace Insim\Packets;

/**
 * Player spectate
 */
class isPLL extends Packet {

    const PACK = 'CCxC';
    const UNPACK = 'CSize/CType/CReqI/CPLID';

    protected $Size = 4;        # 4
    protected $Type = Packet::ISP_PLL;  # ISP_PLL
    protected $ReqI = null;     # 0
    public $PLID;               # player's unique id

}
