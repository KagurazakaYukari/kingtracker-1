<?php

namespace Insim\Packets;

/**
 * Message
 */
class isMST extends Packet {

    const PACK = 'CCxxa64';
    const UNPACK = 'CSize/CType/CReqI/CZero/a64Msg';

    protected $Size = 68;       # 68
    protected $Type = Packet::ISP_MST;  # ISP_MST
    protected $ReqI = 0;        # 0
    protected $Zero = null;
    public $Msg;                # last byte must be zero
}