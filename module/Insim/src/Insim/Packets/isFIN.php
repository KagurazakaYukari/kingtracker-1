<?php

namespace Insim\Packets;

/**
 * Button
 */
class isFIN extends Packet {

    const PACK = 'CCxCVVxCCxvv';
    const UNPACK = 'CSize/CType/CReqI/CPLID/VTTime/VBTime/CSpA/CNumStops/CConfirm/CSpB/vLapsDone/vFlags';

    protected $Size = 20;       # 20
    protected $Type = Packet::ISP_FIN;  # ISP_FIN
    protected $ReqI;            # 0
    public $PLID;               # player's unique id (0 = player left before result was sent)
    public $TTime;              # race time (ms)
    public $BTime;              # best lap (ms)
    protected $SpA;
    public $NumStops;           # number of pit stops
    public $Confirm;            # confirmation flags : disqualified etc - see below
    protected $SpB;
    public $LapsDone;           # laps completed
    public $Flags;              # player flags : help settings etc - see below

}
