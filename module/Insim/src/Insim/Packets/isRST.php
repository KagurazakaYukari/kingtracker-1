<?php

namespace Insim\Packets;

/**
 * Race start
 */
class isRST extends Packet {

    const PACK = 'CCCxCCCCa6CCvvvvvv';
    const UNPACK = 'CSize/CType/CReqI/CZero/CRaceLaps/CQualMins/CNumP/CTiming/a6Track/CWeather/CWind/vFlags/vNumNodes/vFinish/vSplit1/vSplit2/vSplit3';
    const RST_TIMING_STANDARD = 64;
    const RST_TIMING_CUSTOM = 128;
    const RST_TIMING_NONE = 192;

    protected $Size = 28;       # 28
    protected $Type = Packet::ISP_RST;  # ISP_RST
    public $ReqI = true;        # 0 unless this is a reply to an TINY_RST request
    protected $Zero = null;
    public $RaceLaps;           # 0 if qualifying
    public $QualMins;           # 0 if race
    public $NumP;               # number of players in race
    public $Timing;             # lap timing (see below)
    public $Track;              # short track name
    public $Weather;
    public $Wind;
    public $Flags;              # race flags (must pit, can reset, etc - see below)
    public $NumNodes;           # total number of nodes in the path
    public $Finish;             # node index - finish line
    public $Split1;             # node index - split 1
    public $Split2;             # node index - split 2
    public $Split3;             # node index - split 3
    public $splitsCount = 0;

    public function unpack($rawPacket) {
        parent::unpack($rawPacket);

        $timing = $this->Timing;

        $this->Timing = ($this->Timing & 192);

        if ($this->Timing == isRST::RST_TIMING_CUSTOM) {
            $this->splitsCount = ($timing & 1) + ($timing & 2);
        }

        if ($this->Timing == isRST::RST_TIMING_STANDARD) {
            if ($this->Split1 != 65535)
                $this->splitsCount++;
            if ($this->Split2 != 65535)
                $this->splitsCount++;
            if ($this->Split3 != 65535)
                $this->splitsCount++;
        }

        return $this;
    }

    public function isReversed() {
        if (strpos($this->Track, 'R') !== false) {
            return true;
        } else {
            return false;
        }
    }

    public function timingToString() {
        switch ($this->Timing) {
            case isRST::RST_TIMING_NONE:
                return 'none';
            case isRST::RST_TIMING_STANDARD:
                return 'standard';
            case isRST::RST_TIMING_CUSTOM:
                return 'custom checkpoints';
        }
    }

    public function raceLapsToString() {
        if ($this->RaceLaps == 0) {
            return $this->QualMins == 0 ? 'practice' : '---';
        } elseif ($this->RaceLaps >= 1 && $this->RaceLaps <= 99) {
            return $this->RaceLaps . ' laps';
        } elseif ($this->RaceLaps >= 100 && $this->RaceLaps <= 190) {
            return (($this->RaceLaps - 100) * 10 + 100) . ' laps';
        } elseif ($this->RaceLaps >= 191 && $this->RaceLaps <= 238) {
            return ($this->RaceLaps - 190) . ' hours';
        }
    }

}

// RaceLaps (rl) : (various meanings depending on range)

// 0       : practice
// 1-99    : number of laps...   laps  = rl
// 100-190 : 100 to 1000 laps... laps  = (rl - 100) * 10 + 100
// 191-238 : 1 to 48 hours...    hours = rl - 190

// HOSTF_CAN_VOTE		1
// HOSTF_CAN_SELECT		2
// HOSTF_MID_RACE		32
// HOSTF_MUST_PIT		64
// HOSTF_CAN_RESET		128
// HOSTF_FCV			256
// HOSTF_CRUISE			512