<?php

use Insim\Packets\CarContOBJ;
use Insim\Packets\isHLV;
use Insim\Packets\Packet;

namespace Insim\Packets;

class isHLV extends Packet {

    const PACK = 'CCCCCCvx8';
    const UNPACK = 'CSize/CType/CReqI/CPLID/CHLVC/xSp1/vTime/x8C';
    const HLVC_GROUND = 0;
    const HLVC_WALL = 1;
    const HLVC_SPEEDING = 4;
    const HLVC_BOUNDS = 5;
    
    protected $Size = 16;       # 16
    protected $Type = Packet::ISP_HLV;  # ISP_HLV
    protected $ReqI = null;     # 0
    public $PLID;               # player's unique id
    public $HLVC;               # 0 : ground / 1 : wall / 4 : speeding / 5 : out of bounds
    private $Sp1;
    public $Time;               # looping time stamp (hundredths - time since reset - like TINY_GTH)
    public $C;

    public function unpack($rawPacket) {
        parent::unpack($rawPacket);

        $this->C = new CarContOBJ(substr($rawPacket, 8, 8));

        return $this;
    }
    
    public function typeToString(){
        switch ($this->HLVC){
            case isHLV::HLVC_GROUND:
                return 'ground';
            case isHLV::HLVC_WALL:
                return 'wall';
            case isHLV::HLVC_SPEEDING:
                return 'speeding';
            case isHLV::HLVC_BOUNDS:
                return 'bounds';
        }
    }

}