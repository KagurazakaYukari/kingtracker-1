<?php

namespace Insim\Packets;

/**
 * Message
 */
class isMTC extends Packet {

    const PACK = 'CCxCCCxxa128';
    const UNPACK = 'CSize/CType/CReqI/CSound/CUCID/CPLID/CSp2/CSp3/a128Text';

    protected $Size = 136;      # 8 + TEXT_SIZE (TEXT_SIZE = 4, 8, 12... 128)
    protected $Type = Packet::ISP_MTC;  # ISP_MTC
    protected $ReqI = 0;        # 0
    public $Sound = null;       # sound effect (see Message Sounds below)
    public $UCID = 0;           # connection's unique id (0 = host / 255 = all)
    public $PLID = 0;           # player's unique id (if zero, use UCID)
    protected $Sp2 = null;
    protected $Sp3 = null;
    public $Text;               # up to 128 characters of text - last byte must be zero

    public function pack() {
        if (strLen($this->Text) > 127) {
            $lastColor = '^8';
            foreach (explode("\n", wordwrap($this->Text, 127, "\n", true)) as $Text) {
                $this->Text($lastColor . $Text)->Send();
                $lastColorPosition = strrpos($Text, '^');
                if ($lastColorPosition !== false) {
                    $lastColor = substr($Text, $lastColorPosition, 2);
                }
                $this->Text(''); # Prevents the last segment of the word wrap from sending twice
            }
        }

        return parent::pack();
    }

}
