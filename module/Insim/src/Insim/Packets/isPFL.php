<?php

namespace Insim\Packets;

/**
 * Player flags changed
 */
class isPFL extends Packet {

    const PACK = 'CCxCvv';
    const UNPACK = 'CSize/CType/CReqI/CPLID/vFlags/vSpare';

    protected $Size = 8;        # 8
    protected $Type = Packet::ISP_PFL;  # ISP_PFL
    protected $ReqI;            # 0
    public $PLID;               # player's unique id
    public $Flags;              # player flags (see below)
    protected $Spare;

}
