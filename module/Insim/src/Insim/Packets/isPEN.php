<?php

namespace Insim\Packets;

/**
 * Penalty
 */
class isPEN extends Packet {

    const PACK = 'CCxCCCCx';
    const UNPACK = 'CSize/CType/CReqI/CPLID/COldPen/CNewPen/CReason/CSp3';

    protected $Size = 8;        # 8
    protected $Type = self::ISP_PEN;  # ISP_PEN
    protected $ReqI;            # 0
    public $PLID;               # player's unique id
    public $OldPen;             # old penalty value (see below)
    public $NewPen;             # new penalty value (see below)
    public $Reason;             # penalty reason (see below)
    protected $Sp3;

}
