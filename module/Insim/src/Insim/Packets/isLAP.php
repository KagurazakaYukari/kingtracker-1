<?php

namespace Insim\Packets;

/**
 * Lap
 */
class isLap extends Packet {

    const PACK = 'CCxCVVvvxCCx';
    const UNPACK = 'CSize/CType/CReqI/CPLID/VLTime/VETime/vLapsDone/vFlags/CSp0/CPenalty/CNumStops/CSp3';

    protected $Size = 20;       # 20
    protected $Type = Packet::ISP_LAP;  # ISP_LAP
    protected $ReqI;            # 0
    public $PLID;               # player's unique id
    public $LTime;              # lap time (ms)
    public $ETime;              # total time (ms)
    public $LapsDone;           # laps completed
    public $Flags;              # player flags
    protected $Sp0;
    public $Penalty;            # current penalty value (see below)
    public $NumStops;           # number of pit stops
    protected $Sp3;

}
