<?php

namespace Insim\Helper;

use Insim\Types\InSimLang;

class InSimHelper {

    /**
     * Converts LFS (ms) time to string
     * @param type $int
     * @return string
     */
    public static function timeToString($int) {
        $sec = floor($int / 1000);
        $ms = $int - floor($sec * 1000);
        $sec -= ($hours = floor($sec / 3600)) * 3600;
        $sec -= ($minutes = floor($sec / 60)) * 60;

        return ($hours > 0 ? $hours . ':' : '') . (sprintf('%02d:%02d.%02d', $minutes, $sec, ($ms / 10)));
    }

    public static function secToString($sec) {
        $sec -= ($hours = floor($sec / 3600)) * 3600;
        $sec -= ($minutes = floor($sec / 60)) * 60;

        return ($hours > 0 ? $hours . ':' : '') . (sprintf('%02d:%02d', $minutes, $sec));
    }

    public static function diffToString($int, &$negative = false) {
        $negative = $int < 0 ? true : false;
        $sign = $int == 0 ? '' : ($negative ? '-' : '+');
        $int = abs($int);
        $sec = floor($int / 1000);
        $ms = $int - floor($sec * 1000);
        $sec -= ($hours = floor($sec / 3600)) * 3600;
        $sec -= ($minutes = floor($sec / 60)) * 60;

        return $sign . ($hours > 0 ? $hours . ':' : '') . ($minutes > 0 ? $minutes . ':' : '') . ("{$sec}.") . (sprintf('%02d', ($ms / 10)));
    }

    public static function stringToTime($string) {
        $t = explode('.', $string);

        $aTime = explode(':', $t[0]);

        if (count($aTime) > 2) {
            $timeString = "{$aTime[0]}:{$aTime[1]}:{$aTime[2]}";
        } else {
            $timeString = "00:{$aTime[0]}:{$aTime[1]}";
        }

        $sec = strtotime($timeString) - strtotime('TODAY');

        return ($sec * 1000) + (intval(@$t[1]) * 10);
    }

    public static function trackNameToLFSW($name) {
        \Zend\Debug\Debug::dump($name);
    }

}
