<?php

namespace Insim\Helper;

class Vector2D {

    public $x = 0;
    public $y = 0;

    public function __construct($x = 0, $y = 0) {
        $this->x = $x;
        $this->y = $y;
    }

    /**
     * Create Vector2D from angle
     * @param type $angle (rad)
     * @return \Insim\Helper\Vector2D
     */
    public static function fromAngle($angle) {
        return new Vector2D(cos($angle), sin($angle));
    }

    /**
     * Create Vector2D from Point2D A to Point2S B
     * @param \Insim\Helper\Point2D $a
     * @param \Insim\Helper\Point2D $b
     * @return \Insim\Helper\Vector2D
     */
    public static function fromPoints(Point2D $a, Point2D $b) {
        return new Vector2D($a->x - $b->x, $a->y - $b->y);
    }

    /**
     * Dot product
     * @param \Insim\Helper\Vector2D $a
     * @param \Insim\Helper\Vector2D $b
     * @return type
     */
    public static function dot(Vector2D $a, Vector2D $b) {
        return ($a->x * $b->x) + ($a->y * $b->y);
    }

    /**
     * Normalize vector
     * @param \Insim\Helper\Vector2D $vec
     * @return \Insim\Helper\Vector2D
     */
    public static function normalize(Vector2D $vec) {
        $length = Vector2D::length($vec);

        if ($length != 0) {
            return new Vector2D($vec->x / $length, $vec->y / $length);
        }

        return $vec;
    }

    /**
     * Vector length
     * @param \Insim\Helper\Vector2D $vec
     * @return type
     */
    public static function length(Vector2D $vec) {
        return sqrt(($vec->x * $vec->x) + ($vec->y * $vec->y));
    }

}
