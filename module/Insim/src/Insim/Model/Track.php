<?php

namespace Insim\Model;

use Application\Model\CoreModel;

class Track extends CoreModel{
    public $id;
    public $short_name;
    public $track;
    public $circuit;
    public $licence;
    public $grid;
    public $lenght;
    public $elevation;
    public $asphalt;
    public $reverse;
    public $oval;
    public $rallycross;
    public $carpark;
    public $drag;
    
    public $isReversed;
    public $isOpen;
    public $name;
    public $circuit_number;
    public $LFSWid;

    public function exchangeArray($data) {
        $this->id = (!empty($data['id'])) ? $data['id'] : 0;
        $this->short_name = (!empty($data['short_name'])) ? $data['short_name'] : '';
        $this->track = (!empty($data['track'])) ? $data['track'] : '';
        $this->circuit = (!empty($data['circuit'])) ? $data['circuit'] : '';
        $this->licence = (!empty($data['licence'])) ? $data['licence'] : '';
        $this->grid = (!empty($data['grid'])) ? $data['grid'] : '';
        $this->lenght = (!empty($data['lenght'])) ? $data['lenght'] : '';
        $this->elevation = (!empty($data['elevation'])) ? $data['elevationt'] : '';
        $this->asphalt = $data['asphalt'] == 1 ? true : false;
        $this->reverse = $data['reverse'] == 1 ? true : false;
        $this->oval = $data['oval'] == 1 ? true : false;
        $this->rallycross = $data['rallycross'] == 1 ? true : false;
        $this->carpark = $data['carpark'] == 1 ? true : false;
        $this->drag = $data['drag'] == 1 ? true : false;
    }

    public function exchangeObject() {  }
}