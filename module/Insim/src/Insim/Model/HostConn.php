<?php

namespace Insim\Model;

use Application\Model\CoreModel;

class HostConn extends CoreModel{
    public $host_id;
    public $UCID;
    public $UName;
    public $PName;
    public $PName_utf;
    public $licence;
    public $lang;
    public $UserID;
    public $IPAddress;
    public $added;
    public $PLID;
    public $PType;
    public $Flags;
    public $Flags_string;
    public $CName;
    public $SetF;

    public $country;
    public $gmt;
    public $role;

    public function exchangeArray($data) {
        $this->host_id = (!empty($data['host_id'])) ? $data['host_id'] : null;
        $this->UCID = (!empty($data['UCID'])) ? $data['UCID'] : null;
        $this->UName = (!empty($data['UName'])) ? $data['UName'] : null;
        $this->PName = (!empty($data['PName'])) ? $data['PName'] : null;
        $this->PName_utf = (!empty($data['PName_utf'])) ? $data['PName_utf'] : null;
        $this->lang = (!empty($data['lang'])) ? $data['lang'] : null;
        $this->UserID = (!empty($data['UserID'])) ? $data['UserID'] : null;
        $this->IPAddress = (!empty($data['IPAddress'])) ? $data['IPAddress'] : null;
        $this->added = (!empty($data['added'])) ? $data['added'] : null;
        
        $this->PLID = (!empty($data['PLID'])) ? $data['PLID'] : null;
        $this->PType = (!empty($data['PType'])) ? $data['PType'] : null;
        $this->Flags = (!empty($data['Flags'])) ? $data['Flags'] : null;
        $this->Flags_string = (!empty($data['Flags_string'])) ? $data['Flags_string'] : null;
        $this->CName = (!empty($data['CName'])) ? $data['CName'] : null;
        $this->SetF = (!empty($data['SetF'])) ? $data['SetF'] : null;
        
        $this->country = (!empty($data['country'])) ? $data['country'] : '';
        $this->gmt = (!empty($data['gmt'])) ? $data['gmt'] : '';
        $this->role = (!empty($data['role'])) ? $data['role'] : 'guest';
        $this->licence = (!empty($data['licence'])) ? $data['licence'] : '';
    }

    public function exchangeObject() {
        $data = array();
        $data['host_id'] = (!empty($this->host_id)) ? $this->host_id : null;
        $data['UCID'] = (!empty($this->UCID)) ? $this->UCID : null;
        $data['UName'] = (!empty($this->UName)) ? $this->UName : null;
        $data['PName'] = (!empty($this->PName)) ? $this->PName : null;
        $data['PName_utf'] = (!empty($this->PName_utf)) ? $this->PName_utf : null;
        $data['lang'] = (!empty($this->lang)) ? $this->lang : null;
        $data['UserID'] = (!empty($this->UserID)) ? $this->UserID : null;
        $data['IPAddress'] = (!empty($this->IPAddress)) ? $this->IPAddress : null;
        $data['added'] = (!empty($this->added)) ? $this->added : null;
        return $data;
    }

}