<?php

namespace Insim\Model;

use Application\Model\CoreModel;

class Task extends CoreModel{
    const TASK_LFSW_UPDATE_PUBSTAT = 'TASK_LFSW_UPDATE_PUBSTAT';
    const TASK_ACL_UPDATE = 'TASK_ACL_UPDATE';
    
    public $id;
    public $task;
    public $count;

    public function exchangeArray($data) {
        $this->id = (!empty($data['id'])) ? $data['id'] : null;
        $this->task = (!empty($data['task'])) ? $data['task'] : null;
        $this->count = (!empty($data['count'])) ? $data['count'] : null;
    }

    public function exchangeObject() {
        $data = array();
        $data['id'] = (!empty($this->id)) ? $this->id : null;
        $data['task'] = (!empty($this->task)) ? $this->task : null;
        return $data;
    }

}