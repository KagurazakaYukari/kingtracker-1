<?php

namespace Insim\Model;

use Application\Model\CoreModel;

class LFSWPB extends CoreModel{
    public $id;
    public $UName;
    public $PName;
    public $track;
    public $car;
    public $splits;
    public $splits_string;
    public $time;
    public $time_string;
    public $lapcount;
    public $fuel;

    public $timestamp;
    public $last_update;

    public function exchangeArray($data) {
        $this->id = (!empty($data['id'])) ? $data['id'] : 0;
        $this->car = (!empty($data['car'])) ? $data['car'] : null;
        $this->track = (!empty($data['track'])) ? $data['track'] : null;
        $this->splits = (!empty($data['splits'])) ? json_decode($data['splits'], true) : null;
        $this->splits_string = (!empty($data['splits_string'])) ? json_decode($data['splits_string'], true) : null;
        $this->time = (!empty($data['time'])) ? $data['time'] : null;
        $this->time_string = (!empty($data['time_string'])) ? $data['time_string'] : null;
        $this->UName = (!empty($data['UName'])) ? trim($data['UName']) : null;
        $this->PName = (!empty($data['PName'])) ? trim($data['PName']) : null;
        $this->lapcount = (!empty($data['lapcount'])) ? $data['lapcount'] : null;
        $this->fuel = (!empty($data['fuel'])) ? $data['fuel'] : null;
        $this->timestamp = (!empty($data['timestamp'])) ? $data['timestamp'] : null;
        $this->last_update = (!empty($data['last_update'])) ? $data['last_update'] : null;
    }

    public function exchangeObject() {
        $data = array();
        $data['id'] = (!empty($this->id)) ? $this->id : 0;
        $data['car'] = (!empty($this->car)) ? $this->car : null;
        $data['track'] = (!empty($this->track)) ? $this->track : null;
        $data['splits'] = (!empty($this->splits)) ? json_encode($this->splits) : null;
        $data['splits_string'] = (!empty($this->splits_string)) ? json_encode($this->splits_string) : null;
        $data['time'] = (!empty($this->time)) ? $this->time : null;
        $data['time_string'] = (!empty($this->time_string)) ? $this->time_string : null;
        $data['UName'] = (!empty($this->UName)) ? $this->UName : null;
        $data['lapcount'] = (!empty($this->lapcount)) ? $this->lapcount : null;
        $data['fuel'] = (!empty($this->fuel)) ? $this->fuel : null;
        $data['timestamp'] = (!empty($this->timestamp)) ? $this->timestamp : null;
        $data['last_update'] = date('Y-m-d H:i:s');
        
        return $data;
    }
}