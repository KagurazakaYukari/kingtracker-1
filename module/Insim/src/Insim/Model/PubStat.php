<?php

namespace Insim\Model;

use Application\Model\CoreModel;

class PubStat extends CoreModel {

    public $id;
    public $key;

    public function exchangeArray($data) {
        $this->id = (!empty($data['id'])) ? $data['id'] : null;
        $this->key = (!empty($data['key'])) ? trim($data['key']) : null;
    }

    public function exchangeObject() {
        $data = array();
        $data['id'] = (!empty($this->id)) ? $this->id : null;
        $data['key'] = (!empty($this->key)) ? $this->key : null;
        return $data;
    }

}
