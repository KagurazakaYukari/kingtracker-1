<?php

namespace Insim\Model;

use Application\Model\CoreModel;

class Stint extends CoreModel {

    public $player;
    public $numLaps;
    public $currentLap;
    public $splits;
    public $sectors;
    public $laps;
    public $bestSplits;
    public $bestSectors;
    public $bestLap;
    public $tbLap;
    public $split_diff;
    public $sector_diff;
    public $lap_diff;
    public $finish_sector;
    public $splits_count;

    public function __construct() {
        $this->reset();
    }

    public function reset() {
        $this->numLaps = 0;
        $this->currentLap = 1;
        $this->finish_sector = null;
        $this->splits = [];
        $this->sectors = [];
        $this->laps = [];
        $this->split_diff = null;
        $this->sector_diff = null;
        $this->lap_diff = null;
        $this->bestLap = null;
        $this->tbLap = null;
        $this->bestSectors = [];
        $this->bestSplits = [];
    }

    public function addLap(\Insim\Packets\isLap $lap) {
        if ($lap->LTime == 3600000) {
            return;
        }

        $this->numLaps++;
        $this->currentLap++;
        $this->laps[] = $lap->LTime;

        if (!isset($this->finish_sector)) {
            $this->finish_sector = count($this->sectors) + 1;
        }

        $this->sectors[$this->finish_sector] = $lap->LTime - @$this->splits[$this->finish_sector - 1];
        $this->sector_diff = isset($this->bestSectors[$this->finish_sector]) ? ($this->sectors[$this->finish_sector] - $this->bestSectors[$this->finish_sector]) : 0;
        $this->lap_diff = isset($this->bestLap) ? ($lap->LTime - $this->bestLap) : 0;

        if (!isset($this->bestLap) OR $lap->LTime < $this->bestLap) {
            $this->bestLap = $lap->LTime;
        }

        if (!isset($this->bestSectors[$this->finish_sector]) OR $this->sectors[$this->finish_sector] < $this->bestSectors[$this->finish_sector]) {
            $this->bestSectors[$this->finish_sector] = $this->sectors[$this->finish_sector];
        }

        // Calculate best possible lap if all sectors are saved
        if (count($this->bestSectors) == ($this->splits_count + 1)) {
            $this->tbLap = 0;
            foreach ($this->bestSectors as $bs) {
                $this->tbLap += $bs;
            }
        }
    }

    public function addSplit(\Insim\Packets\isSPX $split) {
        if ($split->STime == 3600000) {
            return;
        }

        $this->splits[$split->Split] = $split->STime;

        $this->sectors[$split->Split] = $split->Split == 1 ? $split->STime : ($split->STime - @$this->splits[$split->Split - 1]);

        $this->split_diff = isset($this->bestSplits[$split->Split]) ? ($this->splits[$split->Split] - $this->bestSplits[$split->Split]) : 0;
        $this->sector_diff = isset($this->bestSectors[$split->Split]) ? ($this->sectors[$split->Split] - $this->bestSectors[$split->Split]) : 0;

        if (!isset($this->bestSplits[$split->Split]) OR $split->STime < $this->bestSplits[$split->Split]) {
            $this->bestSplits[$split->Split] = $split->STime;
        }

        if (!isset($this->bestSectors[$split->Split]) OR $this->sectors[$split->Split] < $this->bestSectors[$split->Split]) {
            $this->bestSectors[$split->Split] = $this->sectors[$split->Split];
        }
                
        // Calculate best possible lap if all sectors are saved
        if (count($this->bestSectors) == ($this->splits_count + 1)) {
            $this->tbLap = 0;
            foreach ($this->bestSectors as $bs) {
                $this->tbLap += $bs;
            }
        }
    }

    public function eventRaceStarted() {
        if ($this->player->settings->get('timebar_reset_race')) {
            $this->reset();
        }

        $this->splits_count = $this->player->host->splitsCount;
        $this->currentLap = 1;
    }

    public function eventPlayersSpectated() {
        if ($this->player->settings->get('timebar_reset_spectate')) {
            $this->reset();
        }

        $this->currentLap = 1;
    }

    public function eventTrackChanged() {
        $this->reset();
    }

    public function eventCarResetEvent() {
        if ($this->player->settings->get('timebar_reset_car')) {
            $this->reset();
        }
    }

    public function exchangeArray($data) {
        
    }

    public function exchangeObject() {
        
    }

}
