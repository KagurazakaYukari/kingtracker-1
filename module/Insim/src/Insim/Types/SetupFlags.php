<?php

namespace Insim\Types;

class SetupFlags{
    const TC_ENABLE = 2;
    const ABS_ENABLE = 4;
}