<?php

namespace Insim\Types;

class ButtonStyles {

    /**
     * click this button to send IS_BTC
     */
    const ISB_CLICK = 8;

    /**
     * light button
     */
    const ISB_LIGHT = 16;

    /**
     * dark button
     */
    const ISB_DARK = 32;

    /**
     * align text to left
     */
    const ISB_LEFT = 64;

    /**
     * align text to right
     */
    const ISB_RIGHT = 128;

    const COLOUR_LIGHT_GREY = 0;
    
    const COLOUR_YELLOW = 1;
    const COLOUR_BLACK = 2;
    const COLOUR_WHITE = 3;
    const COLOUR_GREEN = 4;
    const COLOUR_RED = 5;
    const COLOUR_BLUE = 6;
    const COLOUR_GREY = 7;
    
}
