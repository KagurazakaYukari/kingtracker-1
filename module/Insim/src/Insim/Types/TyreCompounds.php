<?php

namespace Insim\Types;

class TyreCompounds {

    const TYRE_R1 = 0;
    const TYRE_R2 = 1;
    const TYRE_R3 = 2;
    const TYRE_R4 = 3;
    const TYRE_ROAD_SUPER = 4;
    const TYRE_ROAD_NORMAL = 5;
    const TYRE_HYBRID = 6;
    const TYRE_KNOBBLY = 7;

}
