<?php

return array(
    'Blackwood' => array('LFSW' => 0, 'shortName' => 'BL'),
    'South City' => array('LFSW' => 1, 'shortName' => 'SO'),
    'Fern Bay' => array('LFSW' => 2, 'shortName' => 'FE'),
    'Autocross' => array('LFSW' => 3, 'shortName' => 'AU'),
    'Kyoto Ring' => array('LFSW' => 4, 'shortName' => 'KY'),
    'Westhill' => array('LFSW' => 5, 'shortName' => 'WE'),
    'Aston' => array('LFSW' => 6, 'shortName' => 'AS'),
    'Rockingham' => array('LFSW' => 7, 'shortName' => 'RO'),
);
