<?php

return array(
    'debug' => array(
        'title' => 'Debug',
        'params' => '<timers>',
        'format' => array(
            'type' => 'string',
        ),
        'required' => 1,
        'hidden' => 1, 'noresend' => 1,
        'callback' => function ($parent, $options) {
    $parent->player->ui->showDebug($options);
}
    ),
    'status' => array(
        'title' => 'Servers status',
        'params' => '',
        'format' => array(),
        'required' => 0,
        'noresend' => 1,
        'list' => 'admin',
        'callback' => function ($parent) {
    $parent->player->ui->showStatus();
}
    ),
    'reload' => array(
        'title' => 'Restart',
        'params' => '',
        'format' => array(),
        'list' => 'admin',
        'required' => 0,
        'noresend' => 1,
        'list' => 'admin',
        'callback' => function ($parent, $options) {
    die();
}
    ),
    'test' => array(
        'title' => 'Test',
        'params' => '<action> [param]',
        'format' => array(
            'action' => 'string',
            'param' => 'string'
        ),
        'required' => 1,
        'hidden' => 1, 'noresend' => 1,
        'callback' => function ($parent, $options) {
    $parent->player->host->hostService->test($options);
}
    ),
    'ver' => array(
        'title' => 'Version',
        'params' => '',
        'format' => array(),
        'list' => 'guest',
        'required' => 0, 'noresend' => 1,
        'callback' => function ($parent) {
    $parent->player->ui->showVersion();
}
    ),
    'help' => array(
        'title' => 'Help',
        'params' => '',
        'format' => array(),
        'list' => 'guest',
        'required' => 0,
        'callback' => function ($parent) {
    $parent->player->ui->showHelp();
}
    ),
    'lh' => array(
        'title' => 'Admin help',
        'params' => '',
        'format' => array(),
        'list' => 'guest',
        'required' => 0,
        'callback' => function ($parent) {
    $parent->player->ui->showHelpAdmin();
}
    ),
    'opt' => array(
        'title' => 'User settings (SHIFT + I)',
        'params' => '',
        'format' => array(),
        'list' => 'guest',
        'required' => 0,
        'callback' => function ($parent) {
    $parent->player->ui->showUserSettings();
}
    ),
    'pl' => array(
        'title' => 'Players list',
        'params' => '',
        'format' => array(),
        'list' => 'guest',
        'required' => 0,
        'callback' => function ($parent) {
    $parent->player->ui->showPlayersList();
}
    ),
    'tr' => array(
        'title' => 'Track / Host informations',
        'params' => '',
        'format' => array(),
        'list' => 'guest',
        'required' => 0,
        'callback' => function ($parent) {
    $parent->player->ui->showTrack();
}
    ),
    'top' => array(
        'title' => 'Top laps ladder',
        'params' => '',
        'format' => array(),
        'list' => 'guest',
        'required' => 0,
        'callback' => function ($parent) {
    $parent->player->ui->showTopLaps();
}
    ),
    'tm' => array(
        'title' => 'Players PBs',
        'params' => '',
        'format' => array(),
        'list' => 'guest',
        'required' => 0,
        'callback' => function ($parent) {
    $parent->player->ui->showPlayersPBs();
}
    ),
    'sc' => array(
        'title' => 'Best theoretical lap in current race',
        'params' => '',
        'format' => array(),
        'list' => 'guest',
        'required' => 0,
        'callback' => function ($parent) {
    $parent->player->ui->showSC();
}
    ),
    'grid' => array(
        'title' => 'Starting grid for next race',
        'params' => '',
        'format' => array(),
        'list' => 'guest',
        'required' => 0,
        'callback' => function ($parent) {
    $parent->player->ui->showGrid();
}
    ),
    'spec' => array(
        'title' => 'Spectate player',
        'params' => '<player name>',
        'format' => array(
            'UName' => 'string',
        ),
        'list' => 'admin',
        'required' => 1, 'noresend' => 1,
        'callback' => function ($parent, $options) {
    $parent->player->playerService->spec($options);
}
    ),
    'kick' => array(
        'title' => 'Kick player',
        'params' => '<player name>',
        'format' => array(
            'UName' => 'string',
        ),
        'list' => 'admin',
        'required' => 1, 'noresend' => 1,
        'callback' => function ($parent, $options) {
    $parent->player->playerService->kick($options);
}
    ),
    'ban' => array(
        'title' => 'Ban player',
        'params' => '<player name> <days> [reason]',
        'format' => array(
            'UName' => 'string',
            'duration' => 'int',
            'reason' => 'string',
        ),
        'list' => 'admin',
        'required' => 2, 'noresend' => 1,
        'callback' => function ($parent, $options) {
    $parent->player->playerService->ban($options);
}
    ),
    'unban' => array(
        'title' => 'Unban player',
        'params' => '<player name>',
        'format' => array(
            'UName' => 'string',
        ),
        'list' => 'admin',
        'required' => 1, 'noresend' => 1,
        'callback' => function ($parent, $options) {
    $parent->player->playerService->unban($options);
}
    ),
    'acl' => array(
        'title' => 'Add admin',
        'params' => '<command> <licence name> [admin level] ',
        'format' => array(
            'cmd' => 'string',
            'UName' => 'string',
            'role' => 'string',
        ),
        'list' => 'dev',
        'required' => 2, 'noresend' => 1,
        'callback' => function ($parent, $options) {
    $parent->player->playerService->aclCmd($options);
}
    ),
    'host' => array(
        'title' => 'Host configuration',
        'params' => '[hostname]',
        'format' => array(
            'hostname' => 'string',
        ),
        'list' => 'admin',
        'required' => 0,
        'hidden' => 0, 'noresend' => 1,
        'callback' => function ($parent, $options) {
    $parent->player->ui->showHost($options);
}
    ),
    'restart' => array(
        'title' => 'Restart race',
        'params' => '[delay in sec]',
        'format' => array(
            'delay' => 'int',
        ),
        'list' => 'admin',
        'required' => 0, 'noresend' => 1,
        'callback' => function ($parent, $options) {
    $delay = isset($options['delay']) ? ($options['delay'] > 3 ? $options['delay'] : 4) : 10;
    $parent->player->host->restartCommand($delay, 'restart');
}
    ),
    'qualify' => array(
        'title' => 'Start qualification',
        'params' => '<qualify lenght in minutes max 240> [delay in sec]',
        'format' => array(
            'lenght' => 'int',
            'delay' => 'int',
        ),
        'list' => 'admin',
        'required' => 1, 'noresend' => 1,
        'callback' => function ($parent, $options) {
    $delay = isset($options['delay']) ? ($options['delay'] > 3 ? $options['delay'] : 4) : -1;
    $lenght = abs($options['lenght']);
    $lenght = $lenght > 240 ? 240 : $lenght;
    $parent->player->host->restartCommand($delay, 'qualify', $lenght);
}
    ),
    'end' => array(
        'title' => 'End race',
        'params' => '[delay in sec]',
        'format' => array(
            'delay' => 'int',
        ),
        'list' => 'admin',
        'required' => 0, 'noresend' => 1,
        'callback' => function ($parent, $options) {
    $delay = isset($options['delay']) ? ($options['delay'] > 3 ? $options['delay'] : 4) : 10;
    $parent->player->host->restartCommand($delay, 'end');
}
    ),
    'cv' => array(
        'title' => 'Cancel vote or restart',
        'params' => '',
        'format' => array(),
        'list' => 'admin',
        'required' => 0, 'noresend' => 1,
        'callback' => function ($parent, $options) {
    $parent->player->host->hostService->cancelVote();
}
    ),
    'laps' => array(
        'title' => 'Set race laps',
        'params' => '<number of laps>',
        'format' => array(
            'laps' => 'int',
        ),
        'list' => 'admin',
        'required' => 1, 'noresend' => 1,
        'callback' => function ($parent, $options) {
    $laps = abs($options['laps']);
    $parent->player->host->hostService->setLaps($laps);
}
    ),
    'hours' => array(
        'title' => 'Set race hours',
        'params' => '<number of hours>',
        'format' => array(
            'hours' => 'int',
        ),
        'list' => 'admin',
        'required' => 1, 'noresend' => 1,
        'callback' => function ($parent, $options) {
    $hours = abs($options['hours']);
    $parent->player->host->hostService->setHours($hours);
}
    ),
    'reorder' => array(
        'title' => 'Reorder starting grid (Race setup screen only)',
        'params' => '',
        'format' => array(),
        'list' => 'admin',
        'required' => 0,
        'callback' => function ($parent) {
    $parent->player->host->hostService->reorderGrid();
}
    ),
    'recent' => array(
        'title' => 'Recently disconnected players',
        'params' => '',
        'format' => array(),
        'list' => 'admin',
        'required' => 0, 'noresend' => 1,
        'callback' => function ($parent) {
    $parent->player->ui->showRecentPlayersList();
}
    ),
    'axlist' => array(
        'title' => 'List of available layouts',
        'params' => '[Track code]',
        'format' => array(
            'X' => 'string',
        ),
        'list' => 'admin',
        'required' => 0, 'noresend' => 1,
        'callback' => function ($parent, $options) {
    $parent->player->ui->showLayouts($options);
}
    ),
);
