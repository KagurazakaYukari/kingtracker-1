# KingTracker #

## Installation ##
###1a Windows ###
- unpack downloaded archive to separate folder
- run UniController.exe (cancel when asked for password) and start Apache and MySQL services
- click on View www

###1b Linux ###
- install LAMP
- Apache 2.4, PHP 7, MySQL 5.6, PHPMyAdmin
- Enabled extension: php_mysql, php_curl, php_mbstring, php_sockets, php_xmlrpc
(sudo apt-get install php7.0-mysql, ...)
- unpack downloaded archive to \var\www\html\
- create kingtracker database (open url http://localhost/phpmyadmin/)
- database tab -> name: kingtracker collation: utf_8_general_ci
- open database and import file \var\www\html\db-install\kingtracker...zip
- change database connection info in \var\www\html\config\autoload\
global.php - dbname=kingtracker;host=localhost
local.php - 'username' => 'root', 'password' => ''
- open url http://localhost/public/index.php/

###2 Login to admin web UI ###
- Default login: admin password (can be changed after login, in profile menu)

###3 configuration ###
- edit or add new hosts
- add admins
- add LFSW pubstat keys
- Settings / Master Server - disable or enable automatic updates 

###4 start kingtracker script ###
- on windows start kingtracker.bat
- on linux php -e /var/www/html/public/index.php insim start -d

## Credits ##
- [DANIEL-CRO - Draft detection](https://www.lfs.net/forum/thread/85771)
- [PHPInSimMod (PRISM) - packets definitions, packets decoding](https://www.lfs.net/forum/312-PHPInSimMod---PRISM)