<?php

$base_path = realpath(dirname(__DIR__));
chdir($base_path);

require 'init_autoloader.php';
require 'vendor/lfs/LFSString.php';

use Zend\Loader\AutoloaderFactory;
use Zend\Mvc\Application;

// Setup autoloader
AutoloaderFactory::factory();

// Get application stack configuration
$configuration = include 'config/application.config.php';

// Init app
$app = @Application::init($configuration);
$sm = $app->getServiceManager();

// Get results service
$resultService = $sm->get('Insim\Service\ResultService');
$resultService->debugSQL = true; // show SQL query
// Get all events (laps / splits)
$events = $resultService->fetchAllEvents($where = [ 'event' => Insim\Model\ResultEvent::LAP], $buffered = true, $tableGateway = false, $order = 'results_event.time DESC', $limit = null);

foreach ($events as $event) {
    Zend\Debug\Debug::dump($event);
}
